﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StClient
{
    public class Utl
    {
        public static bool GetBitFromByteArray(byte[] _in, int pos) { return ((_in[Convert.ToInt32(pos / 8)] & (byte)(1 << (pos % 8))) != 0); }

        public static bool getBitOfInt(int i, int bitNumber) { return (i & (1 << bitNumber)) != 0; }

        public static bool IsOn(int Value, byte Bit) { return (Value >> Bit & 1) == 1; }
        public static int SetBitOfInt(int Value, byte Bit) { return SetBitOfInt(Value, Bit, true); }
        public static int SetBitOfInt(int Value, byte Bit, bool On) { return On ? Value | (1 << Bit) : ClearBitOfInt(Value, Bit); }
        public static int ClearBitOfInt(int Value, byte Bit) { return Value & ~(1 << Bit); }
        //public static void printStack(MotorLogic ml)/////////skinuo2011
        public static void printStack(string desc)
        {
            //return;

            StackTrace st = new StackTrace();
            StackFrame[] sf = st.GetFrames();
            //Debug.WriteLine("*********");
            //Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} stack=", DateTime.Now));
            string s = "";
            int max = Math.Min(6, sf.Length);
            //for (int i = 1; i < max; i++) Debug.Write(sf[i].GetMethod().Name + " ,");
            for (int i = 1; i < max; i++) s += sf[i].GetMethod().Name + " ,";

            Debug.WriteLine("{0:hh:mm:ss.fff} {1} stack={2}", DateTime.Now, desc, s);

            //Debug.WriteLine("{0:hh:mm:ss.fff}>{1,15} id={2,3} state={3,10} cp={4,7} cpold={5,7}  deltaCp={6,5} sbc={7} mov={8,6} stack={9}",
            //    DateTime.Now, ml.Title, ml.MotorID, ml.state, ml.CP, ml.previousCP, Math.Abs(ml.CP - ml.previousCP), ml.SbcLock, ml.Moving, BitConverter.ToInt32(ml.sbc.PDO_CanMsgID, 0), s);

            //Debug.WriteLine("*********");
        }
    }
}

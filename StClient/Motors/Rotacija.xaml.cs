﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
//using System.Windows.Controls.Primitives.

namespace StClient
{
    /// <summary>
    /// Interaction logic for Rotacija.xaml
    /// </summary>
    public partial class Rotacija : UserControl
    {
        Commands commands = new Commands();
        public Rotacija()
        {
            InitializeComponent();

            DataContext = Sys.vagoniRxTx;
            c01.DataContext = c02.DataContext = c03.DataContext = c04.DataContext = c05.DataContext = c06.DataContext = c07.DataContext = c08.DataContext = Sys.vagoniRxTx;
            c34.DataContext = c35.DataContext = Sys.vagoniRxTx;
            //c11.DataContext = c12.DataContext = c14.DataContext = Sys.vagoniRxTx;

            AutoPanel.DataContext = Sys.vagoniRxTx;

            RotInn.DataContext = Sys.vagoniRxTx.RotInn;
            RotOut.DataContext = Sys.vagoniRxTx.RotOut;
        }

        private void ToggleBB_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = true; }
        private void ToggleBB_MouseUp(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = false; }

        private void SP_Inn_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate()
            //TextBlock tb = sender as TextBlock;
            //commands.SP_Rot_Popup(tb);
            //BindingExpression a = tb.GetBindingExpression(TextBlock.TextProperty);
            //BindingExpression b = BindingOperations.GetBindingExpression(tb, TextBlock.TextProperty);
            //BindingExpression c = b.ge;


            commands.SP_Rot_Popup((TextBlock)sender);
            Sys.vagoniRxTx.UpdateRotSP();
            //a.UpdateTarget();
            //a.UpdateSource();
            //b.UpdateSource();
            //b.UpdateSource();
            //Log.Write(string.Format("RotInner: SP changed to {4}.", Sys.vagoniRxTx.RotInn.));
        }
        private void SP_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.SP_Rot_Popup((TextBlock)sender);
        }
        private void StopRot(object sender, RoutedEventArgs e)
        {
            Sys.vagoniRxTx.rotStop();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using LogAlertHB;
using System.Net.Sockets;
using System.Windows.Controls.Primitives;
using System.Diagnostics;


namespace StClient
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class MotorGUI : UserControl
    {
        public MotorLogic _motor;
        //List<MotorLogic> motors;
        Commands commands = new Commands();
        SoloCommands soloCmd = new SoloCommands();
        PopupWindows popups = new PopupWindows();

        public MotorGUI(MotorLogic motor)
        {
            InitializeComponent();
            this.DataContext = motor;
            _motor = motor;
            //motors = _motor.toList();

            //mora za svaki motorlogic da se napravi unos u dictionary (=mnogo CPU)
            //CmdMan.AddButtonInstance(CmdMan.cmdType.soloAuto, Auto);
            //CmdMan.AddButtonInstance(CmdMan.cmdType.soloStop, Stop);
            //CmdMan.AddButtonInstance(CmdMan.cmdType.soloManRelease, Release);
            //CmdMan.AddButtonInstance(CmdMan.cmdType.soloManPos, Up);
            //CmdMan.AddButtonInstance(CmdMan.cmdType.soloManNeg, Down);
            //CmdMan.AddButtonInstance(CmdMan.cmdType.soloResTrip, resetTrip);

            for (byte i = 0; i < Properties.Settings.Default.NumberOfGroups; i++)
                combo.Items.Add(i.ToString());

        }

        private void Release_Click(object sender, RoutedEventArgs e)
        {
            soloCmd.ManRelease(_motor);
            //commands.ManualStarting(motors);
            //Log.Write(string.Format("motorGUI id={0} title={1}: RELEASE BRK pressed", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: RELEASE BRK pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
    _motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);

        }

        private void Up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            soloCmd.ManPos_press(_motor);
            //commands.Up_PreviewMouseLeftButtonDown(motors);
            Log.Write(string.Format("motorGUI: MAN UP pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
    _motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
            //Log.Write(string.Format("motorGUI id={0} title={1}: MAN UP pressed", _motor.MotorID, _motor.Title), EventLogEntryType.Information);

        }

        private void Up_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            soloCmd.ManPos_release(_motor);
            //commands.UpDown_PreviewMouseLeftButtonUp(motors);
            //Log.Write(string.Format("motorGUI: MAN UP released", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: MAN UP released, motor ID={0}, title={1}, cp={2}, cv={3}.",
  _motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
        }

        private void Down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            soloCmd.ManNeg_press(_motor);
            //commands.Down_PreviewMouseLeftButtonDown(motors);
            //Log.Write(string.Format("motorGUI: MAN DOWN pressed", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: MAN DOWN pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
_motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
        }

        private void Down_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            soloCmd.ManNeg_release(_motor);
            //commands.UpDown_PreviewMouseLeftButtonUp(motors);
            //Log.Write(string.Format("motorGUI: MAN DOWN released", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: MAN DOWN pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
_motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
        }

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            soloCmd.ResetTrip(_motor);
            //commands.resetTrip(motors);
            //Log.Write(string.Format("motorGUI: TRIP RST pressed", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: TRIP RST pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
_motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
        }

        private void Auto_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            soloCmd.Auto(_motor);
            //commands.AutoStarting(motors, _motor);
            //commands.SoloAuto(_motor);
            //Log.Write(string.Format("motorGUI: AUTO pressed", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: AUTO pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
_motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
        }

        private void Stop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            soloCmd.Stop(_motor);
            //commands.Stop(motors);
            //Log.Write(string.Format("motorGUI: STOP pressed", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: STOP pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
    _motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);

        }

        private void resetGroup_Click(object sender, RoutedEventArgs e)
        {
            soloCmd.ResetGroup(_motor);
            //commands.ResetGroup(this);
            //Log.Write(string.Format("motorGUI: GRP RST pressed", _motor.MotorID, _motor.Title), EventLogEntryType.Information);
            Log.Write(string.Format("motorGUI: GRP RST pressed, motor ID={0}, title={1}, cp={2}, cv={3}.",
 _motor.MotorID, _motor.Title, _motor.CP, _motor.CV), EventLogEntryType.Information);
        }

        private void SP_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_motor.SpSvEnabled) return;
            popups.SP_Popup(_motor, sender as TextBlock, false);
            Log.Write(string.Format("motorGUI: SP changed to {4}, motor ID={0}, title={1}, cp={2}, cv={3}.",
                _motor.MotorID, _motor.Title, _motor.CP, _motor.CV, _motor.SP), EventLogEntryType.Information);
        }

        private void SV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_motor.SpSvEnabled) return;
            popups.SVMV_Popup(_motor, sender as TextBlock, false, ShowPercButtons.Show);
            Log.Write(string.Format("motorGUI: SV changed to {4}, motor ID={0}, title={1}, cp={2}, cv={3}.",
                _motor.MotorID, _motor.Title, _motor.CP, _motor.CV, _motor.SV), EventLogEntryType.Information);
        }

        private void MV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_motor.SpSvEnabled) return;
            popups.SVMV_Popup(_motor, sender as TextBlock, false, ShowPercButtons.Show);
            Log.Write(string.Format("motorGUI: MV changed to {4}, motor ID={0}, title={1}, cp={2}, cv={3}.",
                _motor.MotorID, _motor.Title, _motor.CP, _motor.CV, _motor.MV), EventLogEntryType.Information);
        }
    }

}
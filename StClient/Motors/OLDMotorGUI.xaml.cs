﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Windows.Threading;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using LogAlertHB;
using System.Net.Sockets;
using System.Windows.Controls.Primitives;


namespace StClient
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class MotorGUI : UserControl
    {
        public MotorLogic _motor;
        List<MotorLogic> motors;
        Commands commands = new Commands();

        public MotorGUI(MotorLogic motor)
        {
            InitializeComponent();
            this.DataContext = motor;
            _motor = motor;
            motors = _motor.toList();
           

            for (byte i = 0; i < Properties.Settings.Default.NumberOfGroups; i++)
                combo.Items.Add(i.ToString());

        }

        private void Release_Click(object sender, RoutedEventArgs e)
        {
            commands.ManualStarting(motors,_motor);           
        }

        private void Up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.Up_PreviewMouseLeftButtonDown(motors);
        }

        private void Up_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            commands.Up_PreviewMouseLeftButtonUp(motors);
        }

        private void Down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.Down_PreviewMouseLeftButtonDown(motors);
        }

        private void Down_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            commands.Down_PreviewMouseLeftButtonUp(motors);
        }

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            commands.resetTrip(motors);
        }

        private void Auto_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.AutoStarting(motors,_motor);      
        }

        private void Stop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.Stop(motors);
        }

        private void resetGroup_Click(object sender, RoutedEventArgs e)
        {
            commands.ResetGroup(this);
        }

        private void SP_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.SP_Popup(motors, sender as TextBlock, false);
        }

        private void SV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.SVMV_Popup(motors, sender as TextBlock, false);
        }

        private void MV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            commands.SVMV_Popup(motors, sender as TextBlock, false);
        }
    }
   
}
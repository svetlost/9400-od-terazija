﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Threading;

namespace StClient.Wagons
{
    public class VagonLogic003 : INotifyPropertyChanged, ILock, ICpCv, ILinkCheck
    {
        //Socket sockRx, sockTx;
        //IPEndPoint ipepClient, ipepPlc;
        //EndPoint epClient;
        //DispatcherTimer timerRx, timerTx;
        //public const int msgLength = 32;
        //byte[] data = new byte[msgLength];
        //byte[] dataTx = new byte[msgLength];

        double _cp, _cv;
        string _Title;
        double _sp = 0;
        double _sv = 100;
        public UInt16 status1 { get; set; }
        public UInt16 status2 { get; set; }
        public int state { get; set; }
        int _Counter = 0;

        public string Title { get { return _Title; } set { if (_Title != value) { _Title = value; OnNotify("Title"); } } }
        public double CP { get { return _cp; } set { if (_cp != value) { _cp = value; OnNotify("CP"); } } }
        public double CP_Absolute { get; set; }
        public double CV { get { return _cv; } set { if (_cv != value) { _cv = value; OnNotify("CV"); } } }
        public double SP { get { return _sp; } set { if (_sp != value) { _sp = value; OnNotify("SP"); } } }
        public double SV { get { return _sv; } set { if (_sv != value) { _sv = value; OnNotify("SV"); } } }
        public double commandGo0, commandGo1, commandGo2, commandStop, commandGoSP;
        public int Counter { get { return _Counter; } set { if (_Counter != value) { _Counter = value; OnNotify("Counter"); } } }
        bool _brake = false, _trip = false, _inh28 = false, _refok = false, _limneg = false, _limpos = false, _local = false, _moving = false, _can = false;
        public bool Brake { get { return _brake; } set { if (_brake != value) { _brake = value; OnNotify("Brake"); } } }
        public bool Trip { get { return _trip; } set { if (_trip != value) { _trip = value; OnNotify("Trip"); } } }
        public bool Inh28 { get { return _inh28; } set { if (_inh28 != value) { _inh28 = value; OnNotify("Inh28"); } } }
        public bool RefOk { get { return _refok; } set { if (_refok != value) { _refok = value; OnNotify("RefOk"); } } }
        public bool LimNeg { get { return _limneg; } set { if (_limneg != value) { _limneg = value; OnNotify("LimNeg"); } } }
        public bool LimPos { get { return _limpos; } set { if (_limpos != value) { _limpos = value; OnNotify("LimPos"); } } }
        public bool Local { get { return _local; } set { if (_local != value) { _local = value; OnNotify("Local"); } } }
        public bool Moving { get { return _moving; } set { if (_moving != value) { _moving = value; OnNotify("Moving"); } } }
        public bool Can { get { return _can; } set { if (_can != value) { _can = value; OnNotify("Can"); } } }
        bool _fw = false, _bw = false, _grp = false, _tripRst = false;
        public bool FW { get { return _fw; } set { if (_fw != value) { _fw = value; OnNotify("FW"); if (_fw) BW = false; } } }
        public bool BW { get { return _bw; } set { if (_bw != value) { _bw = value; OnNotify("BW"); if (_bw) FW = false; } } }

        long _FW_const, _BW_const;
        public long FW_const { get { return _FW_const; } set { if (_FW_const != value) { _FW_const = value; OnNotify("FW_const"); } } }
        public long BW_const { get { return _BW_const; } set { if (_BW_const != value) { _BW_const = value; OnNotify("BW_const"); } } }
        public bool GRP { get { return _grp; } set { if (_grp != value) { _grp = value; OnNotify("GRP"); } } }
        public bool TripRst { get { return _tripRst; } set { if (_tripRst != value) { _tripRst = value; OnNotify("TripRst"); } } }
        //bool _wagAllStop = false, _wagAllAuto = false, _wagRotStop = false, _wagRotAuto = false;
        //public bool wagAllStop { get { return _wagAllStop; } set { if (_wagAllStop != value) { _wagAllStop = value; OnNotify("wagAllStop"); } } }
        //public bool wagAllAuto { get { return _wagAllAuto; } set { if (_wagAllAuto != value) { _wagAllAuto = value; OnNotify("wagAllAuto"); } } }
        //public bool wagRotStop { get { return _wagRotStop; } set { if (_wagRotStop != value) { _wagRotStop = value; OnNotify("wagRotStop"); } } }
        //public bool wagRotAuto { get { return _wagRotAuto; } set { if (_wagRotAuto != value) { _wagRotAuto = value; OnNotify("wagRotAuto"); } } }
        public int CanResponseCount { get; set; }

        bool _Lock;
        public bool Lock { get { return _Lock; } set { if (_Lock != value) { _Lock = value; OnNotify("Lock"); Sys.lockMotors.LocksChanged = true; } } }//safety lock - if motion is not safe block motion

        bool _autoManEnabled;
        public bool autoManEnabled { get { return _autoManEnabled; } set { if (_autoManEnabled != value) { _autoManEnabled = value; OnNotify("autoManEnabled"); } } }//enables motion only if there are no collisions

        int _addr, _tempAddress, i;
        public int Addr { get { return _addr; } }
        int _CanAddr, _CanNet;
        public int CanAddr { get { return _CanAddr; } }
        public int CanNet { get { return _CanNet; } }



        public void PingDevice() { }

        public void ResponseReceived() { _LinkIndicator = Can = true; }

        public void ResetIndicator() { _LinkIndicator = Can = false; }

        bool _LinkIndicator = false;
        public bool LinkIndicator { get { return _LinkIndicator; } set { _LinkIndicator = value; OnNotify("LinkIndicator"); } }

        string _CabinetTitle;
        public string CabinetTitle { get { return _CabinetTitle; } set { _CabinetTitle = value; OnNotify("CabinetTitle"); } }

        public VagonLogic003(int canAddr, int canNet, string title, long _fwconst, long _bwconst, string cabinetTitle)
        {
            _CanAddr = canAddr;
            _CanNet = canNet;
            _Title = title;
            _FW_const = _fwconst;
            _BW_const = _bwconst;
            _CabinetTitle = cabinetTitle;
            CanResponseCount = 0;
        }

        public VagonLogic003(int addr, string title)
        {
            _addr = addr;
            _Title = title;
            CanResponseCount = 0;

            //sockRx = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            //sockTx = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            //ipepClient = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.LocalIp), 3510);
            //epClient = (EndPoint)ipepClient;

            //ipepPlc = new IPEndPoint(IPAddress.Parse("10.4.4.54"), 4500);


            //try { sockRx.Bind(ipepClient); }
            //catch { }

            //timerRx = new DispatcherTimer();
            //timerRx.Tick += timerRx_Tick;
            //timerRx.Interval = TimeSpan.FromMilliseconds(100);
            //timerRx.Start();

            //timerTx = new DispatcherTimer();
            //timerTx.Tick += timerTx_Tick;
            //timerTx.Interval = TimeSpan.FromMilliseconds(200);
            //timerTx.Start();
        }

        //void timerRx_Tick(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        while (sockRx.Available > 0)
        //        {
        //            sockRx.ReceiveFrom(data, ref epClient);

        //            _tempAddress = (ushort)(data[0] + 256 * data[1]);
        //            status1 = (ushort)(data[2] + 256 * data[3]);
        //            status2 = (ushort)(data[4] + 256 * data[5]);
        //            state = (ushort)(data[6] + 256 * data[7]);
        //            //CP = (ushort)(data[8] + 256 * data[9]);
        //            //CV = (ushort)(data[10] + 256 * data[11]);
        //            CP = BitConverter.ToInt16(new byte[] { data[8], data[9] }, 0);
        //            CV = BitConverter.ToInt16(new byte[] { data[10], data[11] }, 0);
        //            //CV = (ushort)(data[10] + 256 * data[11]);
        //            Counter = (ushort)(data[30] + 256 * data[31]);
        //            Trip = getBitOfInt(status1, 2);
        //            Brake = getBitOfInt(status1, 3);
        //            Inh28 = getBitOfInt(status1, 4);
        //            RefOk = getBitOfInt(status1, 5);
        //            LimNeg = getBitOfInt(status1, 6);
        //            LimPos = getBitOfInt(status1, 7);
        //            Local = getBitOfInt(status1, 8);
        //        }
        //    }
        //    catch { };
        //}
        //public static bool getBitOfInt(int i, int bitNumber) { return (i & (1 << bitNumber)) != 0; }
        //void timerTx_Tick(object sender, EventArgs e)
        //{
        //    dataTx[0] = 0; dataTx[1] = 0;//adresa
        //    dataTx[2] = 0; dataTx[3] = 0;
        //    dataTx[4] = 0; dataTx[5] = 0;
        //    dataTx[6] = 0; dataTx[7] = 0;
        //    dataTx[8] = BitConverter.GetBytes(SP)[0]; dataTx[9] = BitConverter.GetBytes(SP)[1];//sp
        //    dataTx[10] = BitConverter.GetBytes(SV)[0]; dataTx[11] = BitConverter.GetBytes(SV)[1]; //sv
        //    dataTx[12] = BitConverter.GetBytes(commandGo0)[0]; dataTx[13] = 0;//park, go0
        //    dataTx[14] = BitConverter.GetBytes(commandGo1)[0]; dataTx[15] = 0;//stage, go1
        //    dataTx[16] = BitConverter.GetBytes(commandGo2)[0]; dataTx[17] = 0;//go prosc (rotation only)
        //    dataTx[18] = BitConverter.GetBytes(commandStop)[0]; dataTx[19] = 0;//stop
        //    dataTx[20] = BitConverter.GetBytes(commandGoSP)[0]; dataTx[21] = 0;//goSP


        //    try { sockTx.SendTo(dataTx, ipepPlc); }
        //    catch { }

        //}

        //public void tstTx()
        //{
        //    try { sockTx.SendTo(new byte[] { (byte)i++, 0, 0 }, ipepPlc); }
        //    catch { }
        //}

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion

    }

    //public class WagonGo : IStopGo, INotifyPropertyChanged
    //{
    //    double _SV;
    //    string _Title;
    //    bool _Enable, _Stop, _Go;

    //    public double SV { get { return _SV; } set { if (_SV != value) { _SV = value; OnNotify("SV"); } } }
    //    public string Title { get { return _Title; } set { if (_Title != value) { _Title = value; OnNotify("Title"); } } }
    //    public bool Enable { get { return _Enable; } set { if (_Enable != value) { _Enable = value; OnNotify("Enable"); } } }
    //    public bool Stop { get { return _Stop; } set { if (_Stop != value) { _Stop = value; OnNotify("Stop"); } } }
    //    public bool Go { get { return _Go; } set { if (_Go != value) { _Go = value; OnNotify("Go"); } } }

    //    #region INotifyPropertyChanged Members
    //    public event PropertyChangedEventHandler PropertyChanged;

    //    private void OnNotify(String parameter)
    //    {
    //        if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(parameter));
    //    }
    //    #endregion
    //}

}

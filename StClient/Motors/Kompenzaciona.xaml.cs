﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StClient
{
    /// <summary>
    /// Interaction logic for Vagoni003.xaml
    /// </summary>
    public partial class Kompenzaciona : UserControl
    {
        //KompenzacionaLogic003 komp { get; set; }
        Commands commands = new Commands();

        public Kompenzaciona(KompenzacionaLogic003 _komp)
        {
            InitializeComponent();

            this.DataContext = _komp;
            //this.komp = _komp;


        }
        public Kompenzaciona()
        {
            InitializeComponent();
        }

        private void ToggleBB_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = true; }
        private void ToggleBB_MouseUp(object sender, MouseButtonEventArgs e) { ((ToggleButton)sender).IsChecked = false; }

        private void resetTrip_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e) { commands.resetTrip(this.DataContext as KompenzacionaLogic003); }
        private void resetTrip_MouseUp(object sender, MouseButtonEventArgs e) { commands.resetTripMouseUp(this.DataContext as KompenzacionaLogic003); }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StClient
{
    /// <summary>
    /// Interaction logic for GraphGui.xaml
    /// </summary>
    public partial class GraphGui2 : UserControl
    {
        List<MotorLogic> motors;

        public GraphGui2(MotorLogic motor)
        {
            InitializeComponent();

            Size gridDesiredSize = grid.DesiredSize;
            this.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            this.Arrange(new Rect(0, 0, gridDesiredSize.Width, gridDesiredSize.Height));

            this.DataContext = motor;
            motors = motor.toList();
            Position.DataContext = motor;
            Position.Max = motor.LimPos;
            Position.Min = motor.LimNeg;
            Position.DisplayMax = motor.LimPos + 50;
            Position.DisplayMin = motor.LimNeg - 50;
            Position.LargeTickNumber = 200;
            Position.SmallTickNumber = 50;
            Position.onStart(motor);
           

        }

        private void Down_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Commands.Instance.Down_PreviewMouseLeftButtonUp(motors);
        }

        private void Down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Commands.Instance.Down_PreviewMouseLeftButtonDown(motors); ;
        }

        private void Up_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Commands.Instance.Up_PreviewMouseLeftButtonUp(motors);
        }

        private void Up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Commands.Instance.Up_PreviewMouseLeftButtonDown(motors);
        }

        private void Release_Click(object sender, RoutedEventArgs e)
        {
            Commands.Instance.Release(motors);
        }

        private void resetTrip_Click(object sender, RoutedEventArgs e)
        {
            Commands.Instance.resetTrip(motors);
        }

        private void MV_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Commands.Instance.SVMV_Popup(motors, sender as TextBlock, false);
        }

        private void Stop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Commands.Instance.Stop_PreviewMouseLeftButtonDown(motors);
        }
    }
}

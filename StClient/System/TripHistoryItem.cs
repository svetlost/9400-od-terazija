﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace StClient
{
    public class TripHistoryItem : INotifyPropertyChanged
    {
        string _Error, _ErrorText;
        int? _Time, _Count;
        public string ErrorText { get { return _ErrorText; } set { if (_ErrorText != value) { _ErrorText = value; OnNotify("ErrorText"); } } }
        public string ErrorCauseRemedy;

        public string Error { get { return _Error; } set { if (_Error != value) { _Error = value; OnNotify("Error"); } } }
        //public int? Time { get { return _Time; } set { if (_Time != value) { _Time = value; OnNotify("Time"); } } }
        //public int? Count { get { return _Count; } set { if (_Count != value) { _Count = value; OnNotify("Count"); } } }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using System.IO;
using System.Windows.Data;
using System.Windows.Markup;
using System.Diagnostics;
using LogAlertHB;
using System.Windows.Input;
using StClient.Properties;
//using StClient.Wagons;
using System.Xml.Serialization;

namespace StClient
{
    public class Sys
    {
        //Unique Instances

        public static TabControl TabControl;
        public static MotorGUI SelectedMotor;
        public static GroupGUI SelectedGroup;
        public static CuesMotorGUI SelectedCuesMotorGUIMotor;
        public static ListBox MotorListbox, GroupListBox, CuesMotorGUIListBox;
        //public static PhidgetItem phitem;
        public static TextBox receivedTxtBlock;
        Commands commands = new Commands();

        //public NkkControl Nkk;

        //static LISTS
        public static List<MotorLogic> StaticMotorList;
        List<MotorLogic> ReadMotorList;
        public static List<GroupLogic> StaticGroupList = new List<GroupLogic>();
        //public static List<DioBrakeCntrl> StaticDioList = new List<DioBrakeCntrl>();
        public static List<DiagItemValues> StaticClusterElectricList = new List<DiagItemValues>();
        public static List<TripXML> StaticListOfTripsLenze9300 = new List<TripXML>();
        public static List<UpDownLogic> UpDownList = new List<UpDownLogic>();
        //public static List<StartecLogic> StaticStartecList = new List<StartecLogic>();
        //public static StartecGroupLogic StartecGroupLogic = new StartecGroupLogic();
        public static List<SecondaryBreakControl> SbcList = new List<SecondaryBreakControl>();
        public static List<ServerLink> ServersList = new List<ServerLink>();
        public static List<StJoystick> joystickList;
        public static List<RfidItem> rfidList;
        public static List<ILinkCheck> linkCheckDeviceList = new List<ILinkCheck>();
        public static List<Cabinet> cabinetList = new List<Cabinet>();
        public static List<Dio1000> DioList = new List<Dio1000>();
        public static List<DrivePlc> DrivePlcList = new List<DrivePlc>();
        public static List<StServer> StServerList = new List<StServer>();
        //public static List<LockPositions> LockPositionsList = new List<LockPositions>();
        public static StServer stServer;
        public static JoyManager joyManager;

        //string path = Environment.CurrentDirectory + @"\Config\";
        static string _pathConfig = Environment.CurrentDirectory + @"\Config\";
        public static string pathConfig { get { return _pathConfig; } set { if (_pathConfig != value) { _pathConfig = value; } } }

        public static byte _KompenzacioneCanNetworkId = (byte)Properties.CanSettings.Default.KompenzacioneCanNetworkID;

        //Class Specific
        GroupLogic _tempGroupLogic;
        //DioBrakeCntrl _dioItem;

        //DispatcherTimer JoyPollTimer = new DispatcherTimer();
        //DispatcherTimer SecBrkControlSendTimer = new DispatcherTimer();
        DispatcherTimer LinkIndicator_SendPing = new DispatcherTimer();
        DispatcherTimer LinkIndicator_ResetIndicator = new DispatcherTimer();
        //public DispatcherTimer DiagItemsUpdateTimer = new DispatcherTimer();
        public DispatcherTimer HoistCPUpdateTimer = new DispatcherTimer();

        //init
        XmlUtil<TripXML> t = new XmlUtil<TripXML>();
        XmlUtil<MotorLogic> x = new XmlUtil<MotorLogic>();
        XmlUtil<UpDownLogic> xmlUpDown = new XmlUtil<UpDownLogic>();
        //XmlUtil<StartecLogic> s = new XmlUtil<StartecLogic>();
        XmlUtil<SecondaryBreakControl> sbc = new XmlUtil<SecondaryBreakControl>();
        XmlUtil<StJoystick> JoystickXml = new XmlUtil<StJoystick>();
        XmlUtil<RfidItem> rfidXml = new XmlUtil<RfidItem>();
        XmlUtil<StServer> StServerXml = new XmlUtil<StServer>();
        XmlUtil<Dio1000> Dio1000Xml = new XmlUtil<Dio1000>();
        XmlUtil<DrivePlc> DrivePlcXml = new XmlUtil<DrivePlc>();
        //XmlUtil<LockPositions> LockPositionsXml = new XmlUtil<LockPositions>();


        //public static LockMotors lockMotors;

        //static VagoniRxTx _vagoniRxTx = new VagoniRxTx();
        //public static VagoniRxTx vagoniRxTx { get { return _vagoniRxTx; } }
        //public static VagoniRxTx vagoniRxTx = new VagoniRxTx();

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        //public static bool LocksEnabled = Properties.Settings.Default.LckEnabled;
        //public static bool LocksOverrideEnabled = Properties.Settings.Default.LckOvrEnabled;

        //double _SvRotInner, _SvRotOuter;
        //public double SvRotInner { get { return _SvRotInner; } set { if (_SvRotInner != value) { _SvRotInner = value; OnNotify("SvRotInner"); } } }
        //public double SvRotOuter { get { return _SvRotOuter; } set { if (_SvRotOuter != value) { _SvRotOuter = value; OnNotify("SvRotOuter"); } } }

        //public static int potSvRotInner;
        //public static int potSvRotOuter;

        public static bool render3D = Properties.Settings.Default.Render3D;
        //public static int movingToFalseCounter = Properties.Settings.Default.MovingToFalseCounter;

        public static string initResult { get; set; }
        public static void AddInitResult(string s) { initResult += s + "\n"; }

        public static string ParseResult(string filename, object list, int itemsImported)
        {
            if (list == null) return "Parsing " + filename + " failed, no records imported.";

            return "Parsing " + filename + " OK, implrted " + itemsImported.ToString() + " items";
        }
        public static string ParseResult2<T>(string filename, List<T> list)
        {
            if (list == null) return "Parsing " + filename + " FAILED, no items imported.";

            return "Parsing " + filename + " OK, imported " + list.Count + " items";
        }



        public Sys()
        {
            if (!Directory.Exists(pathConfig))
                Directory.CreateDirectory(pathConfig);

            //CREATE GROUPS
            for (byte i = 0; i < Properties.Settings.Default.NumberOfGroups; i++)
            {
                _tempGroupLogic = new GroupLogic(i);
                StaticGroupList.Add(_tempGroupLogic);
            }

            //TRIP LIST            
            StaticListOfTripsLenze9300 = t.ReadXml(pathConfig + "tripListL9300-20.xml", CreateDefaultRow.DontCreate);
            AddInitResult(ParseResult2("tripListL9300-20.xml", StaticListOfTripsLenze9300));


            //CREATE upDown
            UpDownList = xmlUpDown.ReadXml(pathConfig + "simple_List.xml", CreateDefaultRow.Create);
            AddInitResult(ParseResult2("simple_List.xml", UpDownList));

            //CREATE HOIST
            //StaticStartecList = s.ReadXml(pathConfig + "hoist_List.xml", CreateDefaultRow.Create);
            //AddInitResult(ParseResult2("hoist_List.xml", StaticStartecList));

            //CREATE MOTORS           
            ReadMotorList = x.ReadXml(pathConfig + "motors_List.xml", CreateDefaultRow.Create);
            AddInitResult(ParseResult2("motors_List.xml", ReadMotorList));

            //CREATE JOYSTICKS
            joystickList = JoystickXml.ReadXml(Sys.pathConfig + "PhJoystick.xml", CreateDefaultRow.Create);
            AddInitResult(ParseResult2("PhJoystick.xml", joystickList));
            //JoyPollTimer.Interval = TimeSpan.FromMilliseconds(Sett.JoystickPollTime_ms);
            //JoyPollTimer.Tick += JoyPollTimer_Tick;
            //if (joystickList.Count > 0) JoyPollTimer.Start();
            if (joystickList.Count > 0)
            {
                joyManager = new JoyManager(joystickList, Settings.Default.JoystickType);
                

            }
            //READ RFIDS
            rfidList = rfidXml.ReadXml(Sys.pathConfig + "Rfids.xml", CreateDefaultRow.Create);
            AddInitResult(ParseResult2("Rfids.xml", rfidList));

            DioList = Dio1000Xml.ReadXml(Sys.pathConfig + "Dio1000.xml", CreateDefaultRow.Create);
            AddInitResult(ParseResult2("Dio1000.xml", DioList));

            DrivePlcList = DrivePlcXml.ReadXml(Sys.pathConfig + "DrvPlc.xml", CreateDefaultRow.Create);
            AddInitResult(ParseResult2("DrvPlc.xml", DrivePlcList));

            //LockPositionsList = LockPositionsXml.ReadXml(Sys.pathConfig + "LockPositions.xml", CreateDefaultRow.Create);
            //AddInitResult(ParseResult2("LockPositions.xml", LockPositionsList));



            //Log.Write(File.ReadAllText(path + "motors_List.xml"));
            StaticMotorList = ReadMotorList.FindAll(m => m.Enabled);

            //setup parent platforms
            //foreach (var mot in ReadMotorList.Where(q => q.ParentMotorID != 0)) mot.ParentMotor = ReadMotorList.Find(w => w.MotorID == mot.ParentMotorID);

            //setup child motors
            //var parentPlatforms = ReadMotorList.Where(w => w.ParentMotorID != 0).GroupBy(p => p.ParentMotorID).Select(q => q.First());
            //foreach (var mmm in parentPlatforms) mmm.ChildMotors.AddRange(ReadMotorList.Where(q => q.ParentMotorID == mmm.MotorID));

            //CREATE SBC PLEC LIST
            SbcList = sbc.ReadXml(pathConfig + "sbc_List.xml", CreateDefaultRow.Create);
            AddInitResult(ParseResult2("sbc_List.xml", SbcList));
            //        foreach (var mot in StaticMotorList) linkCheckDeviceList.Add(mot as ILinkCheck);
            //      linkCheckDeviceList.Add(stServer = new StServer("Server", "ROS", Sett.ServerMainIP, Sett.ServerPort));

            //      foreach (var ert in DioList) linkCheckDeviceList.Add(ert);
            //     foreach (var ert in DrivePlcList) linkCheckDeviceList.Add(ert);
            //       foreach (var ccc in Sys.linkCheckDeviceList.GroupBy(mnm => mnm.CabinetTitle)) cabinetList.Add(new Cabinet(ccc.ToList()));
            //LinkIndicator_SendPing.Interval = TimeSpan.FromMilliseconds(570);
            //LinkIndicator_SendPing.Tick += LinkIndicator_SendPing_Tick;
            //  LinkIndicator_SendPing.Start();

            //LinkIndicator_ResetIndicator.Interval = TimeSpan.FromMilliseconds(1490);
            //LinkIndicator_ResetIndicator.Tick += LinkIndicator_ResetIndicator_Tick;
            //   LinkIndicator_ResetIndicator.Start();

            //send goOperational
            TimedAction.ExecuteWithDelay(new Action(delegate
            {
                foreach (SecondaryBreakControl sbcs in SbcList) UdpTx.UniqueInstance.SendGoOperational(sbcs.CanChannel);
            }), TimeSpan.FromSeconds(4));

            //set sp to cp
            TimedAction.ExecuteWithDelay(new Action(delegate
            {
                foreach (MotorLogic motor in Sys.StaticMotorList.FindAll(mm => mm.Can)) motor.SP = motor.CP;
            }), TimeSpan.FromSeconds(4));

            // if (StaticMotorList.Count > 0) lockMotors = new LockMotors();

            ServersList.Add(new ServerLink("Server MAIN", Properties.Settings.Default.ServerMainIP));
            //ServersList.Add(new ServerLink("Server BACKUP", Properties.Settings.Default.ServerBackupIP));

            ////Diag Items Update TIMER
            //DiagItemsUpdateTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.DiagItemsUpdateTimerInterval);
            //DiagItemsUpdateTimer.Tick += new EventHandler(DiagItemsUpdate);

            //Hoist Items CP Update TIMER
            //HoistCPUpdateTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.HoistCPUpdateTimerInterval);
            //HoistCPUpdateTimer.Tick += new EventHandler(HoistCPUpdateTimer_Tick);

            AddInitResult("Motors parsed:");
            foreach (MotorLogic mm in StaticMotorList) AddInitResult(string.Format("Motor, {0}, id={1}, lim-={2} lim+={3} adr={4}, maxV={5}", mm.Title, mm.MotorID, mm.LimNeg, mm.LimPos, mm.CanAddress, mm.MaxV));

            foreach (MotorLogic motor in StaticMotorList)
            {
                motor.SetMotorDirection();
                CueLogic.UniqueInstance.SubscribeMotorsToCue(motor);
                motor.SubscribeToCueLogic(CueLogic.UniqueInstance);

                motor.sbc = SbcList.Find(qq => qq.CanChannel == motor.CanChannel);


                //_dioItem = StaticDioList.Find(q => q.CanChannel == motor.CanChannel && q.Address == motor.SecBrkControlAddress);

                //if (_dioItem == null)
                //{
                //    _dioItem = new DioBrakeCntrl { Address = (byte)motor.SecBrkControlAddress, CanChannel = motor.CanChannel };
                //    //_dioItem.motorListinDIO.Add(motor);
                //    StaticDioList.Add(_dioItem);
                //}
                //else
                //{
                //    //_dioItem.motorListinDIO.Add(motor);
                //}

                //motor.FillDiagItems();

                if (motor.SdoChannel == 1)
                    motor.SdoCanMsgID = CanSett.SDO1_TX_BaseID;
                else
                    motor.SdoCanMsgID = CanSett.SDO2_TX_BaseID;

                motor.SDO_CanMsgID = BitConverter.GetBytes(motor.SdoCanMsgID + motor.CanAddress);
                //motor.PDO_CanMsgID = BitConverter.GetBytes(CanStructure.PDO2_TX_BaseID + motor.CanAddress);
                motor.PDO_CanMsgID = BitConverter.GetBytes(CanSett.PDO3_TX_BaseID + motor.CanAddress);
                //motor.PDO_CanMsgID = BitConverter.GetBytes(CanSett.PDO2_TX_BaseID + motor.CanAddress);//20170903 terazije probano da se salje na pdo2 ali tu ima problema

                //201102
                ////SBC SENDER TIMER - works only if SBC exists
                //if (StaticDioList.Count > 0)
                //{
                //SecBrkControlSendTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.SBCSendInterval);
                //SecBrkControlSendTimer.Tick += new EventHandler(SecBrkSend);
                //SecBrkControlSendTimer.Start();
                //}

                if (!VisibilityStatesLogic.UniqueInstance.AllClusters.Contains(motor.ClusterLogical))
                    VisibilityStatesLogic.UniqueInstance.AllClusters.Add(motor.ClusterLogical);
            }

            VisibilityStatesLogic.UniqueInstance.FillOnStartup();

            //CLUSTER ELECTRIC
            foreach (byte _ClusterID in StaticMotorList.Select(q => q.ClusterElectric).Distinct())
            {
                DiagItemValues item = new DiagItemValues
                {
                    Cluster = _ClusterID,
                    Text = "Cabinet " + _ClusterID.ToString(),
                    Units = "A"
                };
                StaticClusterElectricList.Add(item);
            }

            // TimedAction.ExecuteWithDelay(new Action(delegate { foreach (var m in Sys.StaticMotorList)m.MV = m.SV = 0.5 * m.MaxV; }), TimeSpan.FromSeconds(3));
            TimedAction.ExecuteWithDelay(new Action(delegate { foreach (var m in Sys.StaticMotorList) m.MV = m.SV = m.DefaultMV; }), TimeSpan.FromSeconds(3));


            //at init end write log string
            Log.Write(initResult, EventLogEntryType.Information);
        }

        //private void JoyPollTimer_Tick(object sender, EventArgs e)
        //{
        //    throw new NotImplementedException();
        //}

        //void LinkIndicator_ResetIndicator_Tick(object sender, EventArgs e)
        //{
        //    foreach (var poi in linkCheckDeviceList) poi.ResetIndicator();
        //}

        //void LinkIndicator_SendPing_Tick(object sender, EventArgs e)
        //{
        //    foreach (var poi in linkCheckDeviceList) poi.PingDevice();
        //}




        //void HoistCPUpdateTimer_Tick(object sender, EventArgs e)
        //{
        //    int index = 1381;
        //    byte subindex = 61;
        //    for (int i = 26; i <= 29; i++)
        //        UdpTx.UniqueInstance.SendSDOReadRQ(i, index, subindex, SdoPriority.med);
        //    //UdpTx.UniqueInstance.SendSDOReadRQ(32, index, subindex, SdoPriority.med);
        //    //UdpTx.UniqueInstance.SendSDOReadRQ(34, index, subindex, SdoPriority.med);
        //    //UdpTx.UniqueInstance.SendSDOReadRQ(38, index, subindex, SdoPriority.med);
        //    //UdpTx.UniqueInstance.SendSDOReadRQ(42, index, subindex, SdoPriority.med);
        //}
        // #endregion

        #region TIMERS
        void SecBrkSend(object sender, EventArgs e)
        {

            //DateTime ddd = DateTime.Now;
            //int data = 0;
            //foreach (DioBrakeCntrl dio in StaticDioList)
            //{
            //foreach (MotorLogic ml in Sys.StaticMotorList)
            //    if (ml.SbcLock == MotorBrake.Unlocked)
            //        data += 1 << ml.SbcBit;

            foreach (SecondaryBreakControl sbc in Sys.SbcList)
                //    if (starteclogic.SecBrkControlLock == MotorBrake.Unlocked)
                //        data += 1 << starteclogic.SecBrkControlBit;

                UdpTx.UniqueInstance.SendPDOSBC(sbc.CanChannel, sbc.PDO_CanMsgID, sbc.Output);
            //}
            //Debug.WriteLine(string.Format("{0} {1}", DateTime.Now.ToLongTimeString(), data));
            //Debug.WriteLine(DateTime.Now.ToLongTimeString() + "  " + (DateTime.Now - ddd).TotalMilliseconds);
        }
        //void DiagItemsUpdate(object sender, EventArgs e)
        //{
        //    foreach (MotorLogic m in StaticMotorList.FindAll(q => q.Can == true))
        //        foreach (DiagItemValues di in m.DiagItemsList)
        //            UdpTx.UniqueInstance.SendSDOReadRQ(m.MotorID, di.LenzeIndex, di.LenzeSubIndex, SdoPriority.lo);

        //}
        #endregion
    }

    public class TripXML
    {
        public int ID { get; set; }
        public string Error { get; set; }
        public string ErrorDescription { get; set; }
        public string CauseRemedy { get; set; }
    }

    public class ServerLink : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public string IpAddress { get; set; }
        int counter = 0;
        bool _LinkOk = false;
        public bool LinkOk { get { return _LinkOk; } set { if (_LinkOk != value) { _LinkOk = value; OnNotify("LinkOk"); } } }
        public void IncrementCounter() { counter++; }
        public void CheckLink() { LinkOk = counter > 0; counter = 0; }

        public ServerLink(string _name, string _IpAdress)
        {
            Name = _name;
            IpAddress = _IpAdress;
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion
    }

}




﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Ports;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows.Threading;
using System.Threading;

namespace SerialTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            nkk = new NkkControl(this.Dispatcher);
            //NkkControl.windowDispacher = this.Dispatcher;

            NkkControl.NkkEvents[0].OnSwitchPress = tstOn;
            NkkControl.NkkEvents[0].OnSwitchRelease = tstOff;
        }

        public static Dispatcher mainWindowDispacher;

        NkkControl nkk;

        public class NkkControl
        {

            public int NkkSwtichCount = Properties.Settings.Default.NkkSwitchCount;
            public static Dispatcher windowDispacher { get; set; }

            private static SerialPort _serialPort = new SerialPort();
            private int _baudRate = 9600;
            private int _dataBits = 8;
            private Handshake _handshake = Handshake.None;
            private Parity _parity = Parity.None;
            private string _portName = "COM1";
            private StopBits _stopBits = StopBits.One;

            public NkkControl(Dispatcher _windowDispacher)
            {
                windowDispacher = _windowDispacher;

                OpenComPort();
                CommInitCheck();

                for (int i = 0; i < NkkSwtichCount; i++) NkkEvents[i] = new NkkButtonEvent();
            }

            System.Threading.Thread thread;
            void TestLedAndText()
            {
                for (int i = 0; i < 7; i++)
                {
                    windowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate() { Write_4N_4N_Chars(i, i.ToString()); });
                    Thread.Sleep(30);
                }

                foreach (NkkColors col in (NkkColors[])Enum.GetValues(typeof(NkkColors)))
                {
                    for (int i = 0; i < 7; i++)
                    {
                        windowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate() { SetColorToButton4(col, i); });
                        Thread.Sleep(30);
                    }
                    Thread.Sleep(300);
                }
                for (int i = 0; i < 7; i++)
                {
                    windowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate()
                    {
                        Write_4N_4N_Chars(i, "");
                        SetColorToButton4(NkkColors.black, i);
                    });
                    Thread.Sleep(30);
                }
            }

            public void TestTextAndColor()
            {
                thread = new System.Threading.Thread(new System.Threading.ThreadStart(TestLedAndText));
                thread.IsBackground = true;
                thread.Start();
            }

            static void send(string s)
            {
                byte[] bb = Encoding.ASCII.GetBytes(s);
                _serialPort.Write(bb, 0, bb.Length);
            }

            static void send(byte[] b) { _serialPort.Write(b, 0, b.Length); }

            public bool CommCheckOk { get; set; }

            public int BaudRate { get { return _baudRate; } set { _baudRate = value; } }
            public int DataBits { get { return _dataBits; } set { _dataBits = value; } }
            public Handshake Handshake { get { return _handshake; } set { _handshake = value; } }
            public Parity Parity { get { return _parity; } set { _parity = value; } }
            public string PortName { get { return _portName; } set { _portName = value; } }
            public bool OpenComPort()
            {
                try
                {
                    _serialPort.BaudRate = _baudRate;
                    _serialPort.DataBits = _dataBits;
                    _serialPort.Handshake = _handshake;
                    _serialPort.Parity = _parity;
                    _serialPort.PortName = _portName;
                    _serialPort.StopBits = _stopBits;
                    _serialPort.DataReceived += new SerialDataReceivedEventHandler(_serialPort_DataReceived);
                    _serialPort.Open();
                }
                catch { return false; }
                return true;
            }

            //public static byte[] intToAscii4(int i)
            //{
            //    string hex = i.ToString("X");
            //    if (hex.Length < 2) hex = "0" + hex;
            //    return Encoding.ASCII.GetBytes(hex);
            //}

            public static byte[] intToAscii5(int i) { return Encoding.ASCII.GetBytes(i.ToString("X2")); }

            public static void BrightnessIncr() { send(new byte[] { 0x26, 0x49, 0x58 }); }
            public static void BrightnessDecr() { send(new byte[] { 0x26, 0x4A, 0x58 }); }

            public enum NkkColors { red = 17, blue = 68, green = 34, cyan = 102, magenta = 85, white = 119, yellow = 51, black = 0 }

            public static void Write_6N_6N_6N_Chars(int buttonNo, string txt) { WriteChars(buttonNo, txt, 0x38, 18); }

            public static void Write_4N_4N_Chars(int buttonNo, string txt) { WriteChars(buttonNo, txt, 0x34, 8); }

            public static void Write_4I_4N_Chars(int buttonNo, string txt) { WriteChars(buttonNo, txt, 0x36, 8); }

            static void WriteChars(int buttonNo, string txt, byte _byCommand, byte _length)
            {
                byte[] byAddress = intToAscii5(buttonNo);
                byte[] byTxt = Encoding.ASCII.GetBytes(normalizeToLength(txt, _length));
                byte[] byCommand = new byte[] { 0x27, byAddress[0], byAddress[1], 0x30, _byCommand };
                byte[] byToSend = Combine(byCommand, byTxt);
                Debug.WriteLine(Encoding.Default.GetString(byToSend));
                send(byToSend);
            }

            public static void SetColorToButton4(NkkColors color, int buttonNo)
            {
                byte[] arg1, arg2;
                arg1 = intToAscii5(buttonNo);
                arg2 = intToAscii5((int)color);
                byte[] ba = new byte[] { 0x2f, 0x30, arg1[0], arg1[1], arg2[0], arg2[1] };
                Debug.WriteLine("{0}{1}{2}{3}{4}{5}", ba[0], ba[1], ba[2], ba[3], ba[4], ba[5]);
                send(ba);
            }

            public static void loadImageToButton4(int imageAddress, int buttonNo)
            {
                byte[] arg1, arg2;
                arg1 = intToAscii5(buttonNo);
                arg2 = intToAscii5(imageAddress);
                byte[] ba = new byte[] { 0x2e, 0x30, arg1[0], arg1[1], arg2[0], arg2[1] };
                Debug.WriteLine("{0:X}{1:X}{2:X}{3:X}{4:X}{5:X}", ba[0], ba[1], ba[2], ba[3], ba[4], ba[5]);
                Debug.WriteLine(Encoding.Default.GetString(ba));
                send(ba);
            }

            public void CommInitCheck() { CommCheckOk = false; send(new byte[] { 0x01 }); }

            static string normalizeToLength(string s, int length) { return s.Length > length ? s.Substring(0, length) : s.PadRight(length); }
            public static byte[] Combine(byte[] first, byte[] second)
            {
                byte[] ret = new byte[first.Length + second.Length];
                Buffer.BlockCopy(first, 0, ret, 0, first.Length);
                Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
                return ret;
            }
            StringBuilder receiveBuffer = new StringBuilder();

            void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
            {
                bool clearBuffer = false;
                int bufSize = 20;
                Byte[] dataBuffer = new Byte[bufSize];
                int count = _serialPort.Read(dataBuffer, 0, bufSize);
                for (int i = 0; i < count; i++) receiveBuffer.Append(String.Format("{0:X}", Convert.ToInt32(dataBuffer[i])));
                Debug.WriteLine(receiveBuffer.ToString());

                var regex = new Regex(@"8[1-6]|B[1-6]|61");
                Match match;
                do
                {
                    match = regex.Match(receiveBuffer.ToString());
                    if (match.Success)
                    {
                        //Console.WriteLine(match.Captures[0].Value);
                        receiveBuffer.Remove(match.Captures[0].Index, match.Captures[0].Length);
                        int _button = Convert.ToInt32(match.Captures[0].Value[1]) - 49;
                        if (match.Captures[0].Value[0] == 56)
                        {
                            Debug.WriteLine(string.Format("button {0} pressed", _button));
                            if (NkkEvents[_button].OnSwitchPress != null)
                                windowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate() { NkkEvents[_button].OnSwitchPress(); });
                        }
                        else if (match.Captures[0].Value[0] == 66)
                        {
                            Debug.WriteLine(string.Format("button {0} released", _button));
                            if (NkkEvents[_button].OnSwitchRelease != null)
                                windowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate() { NkkEvents[_button].OnSwitchRelease(); });
                        }
                        else if (match.Captures[0].Value[0] == 54 && match.Captures[0].Value[1] == 49) CommCheckOk = true;
                        else Debug.WriteLine("???");
                        clearBuffer = true;
                    }
                } while (match.Success);

                if (clearBuffer) receiveBuffer.Clear();

            }

            public class NkkButtonEvent
            {
                public Action OnSwitchPress { get; set; }
                public Action OnSwitchRelease { get; set; }
            }

            public static NkkButtonEvent[] NkkEvents = new NkkButtonEvent[Properties.Settings.Default.NkkSwitchCount];

            public void ClearAll()
            {
                for (int i = 0; i < 16; i++)
                {
                    NkkControl.Write_4N_4N_Chars(i, " ");
                    NkkControl.SetColorToButton4(NkkColors.black, i);
                }
            }

            #region visak
            #endregion


        }

        Random rnd = new Random();
        private void btRed_Click(object sender, RoutedEventArgs e) { NkkControl.SetColorToButton4(NkkControl.NkkColors.red, 1); }
        private void btGreen_Click(object sender, RoutedEventArgs e) { NkkControl.SetColorToButton4(NkkControl.NkkColors.green, 1); }
        //private void btGreen_Click(object sender, RoutedEventArgs e) { spi.send(SerialPortInterface.loadImageToButton4(rnd.Next(1, 11), 1)); }//radiiiiii
        private void btBlack_Click(object sender, RoutedEventArgs e) { NkkControl.SetColorToButton4(NkkControl.NkkColors.black, 1); }
        private void btCyan_Click(object sender, RoutedEventArgs e) { NkkControl.SetColorToButton4(NkkControl.NkkColors.cyan, 1); }
        private void btMagenta_Click(object sender, RoutedEventArgs e) { NkkControl.SetColorToButton4(NkkControl.NkkColors.magenta, 1); }
        private void btWhite_Click(object sender, RoutedEventArgs e) { NkkControl.SetColorToButton4(NkkControl.NkkColors.white, 1); }
        private void btYellow_Click(object sender, RoutedEventArgs e) { NkkControl.SetColorToButton4(NkkControl.NkkColors.yellow, 1); }
        private void btBlue_Click(object sender, RoutedEventArgs e) { NkkControl.SetColorToButton4(NkkControl.NkkColors.blue, 1); }

        private void BrghtIncr(object sender, RoutedEventArgs e) { NkkControl.BrightnessIncr(); }
        private void BrghtDecr(object sender, RoutedEventArgs e) { NkkControl.BrightnessDecr(); }

        private void btTxt666_Click(object sender, RoutedEventArgs e) { NkkControl.Write_6N_6N_6N_Chars(1, tbText.Text); }
        private void btTxt44_Click(object sender, RoutedEventArgs e) { NkkControl.Write_4N_4N_Chars(1, tbText.Text); }
        private void btTxt4I4N_Click(object sender, RoutedEventArgs e) { NkkControl.Write_4I_4N_Chars(1, tbText.Text); }

        private void btRnd_Click(object sender, RoutedEventArgs e) { NkkControl.loadImageToButton4(rnd.Next(1, 11), 1); }
        private void btLoad01to01_Click(object sender, RoutedEventArgs e) { NkkControl.loadImageToButton4(1, 1); }

        private void btGysposy2_Click(object sender, RoutedEventArgs e) { nkk.TestTextAndColor(); }

        void tstOn() { tbTest.IsChecked = true; Debug.WriteLine("tstOn"); }
        void tstOff() { tbTest.IsChecked = false; Debug.WriteLine("tstOff"); }

        private void btClearAll_Click(object sender, RoutedEventArgs e) { nkk.ClearAll(); }

        private void btFillAll_Click(object sender, RoutedEventArgs e)
        {
            NkkControl.Write_4N_4N_Chars(3, "CUE NEXT");
            NkkControl.SetColorToButton4(NkkControl.NkkColors.cyan, 3);
            NkkControl.Write_4N_4N_Chars(2, "CUE PREV");
            NkkControl.SetColorToButton4(NkkControl.NkkColors.cyan, 2);

            NkkControl.Write_4N_4N_Chars(1, "STOPALL");
            NkkControl.SetColorToButton4(NkkControl.NkkColors.red, 1);

            NkkControl.Write_4N_4N_Chars(6, "CUE STOP");
            NkkControl.SetColorToButton4(NkkControl.NkkColors.red, 6);

            NkkControl.Write_4N_4N_Chars(4, "ENA BLE");
            NkkControl.SetColorToButton4(NkkControl.NkkColors.white, 4);
            NkkControl.Write_4N_4N_Chars(5, "CUE AUTO");
            NkkControl.SetColorToButton4(NkkControl.NkkColors.green, 5);
        }











    }
}
﻿using LogAlertHB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace StClient
{
    class GrpCommands
    {
        static string Message;
        PopUpWindow popupWin;

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        public void Auto(GroupLogic _group)
        {
            _group.cmdAuto = true;
            TimedAction.ExecuteWithDelay(new Action(delegate
            { _group.cmdAuto = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));

        }
        public void Stop(GroupLogic _group)
        {
            _group.groupSM.stopMotors();
            //_group.cmdStop = true;
            //TimedAction.ExecuteWithDelay(new Action(delegate
            //{ _group.cmdStop = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));

        }
        public void JoyRelease(GroupLogic _group)
        {
            _group.cmdJoyStart = true;
            TimedAction.ExecuteWithDelay(new Action(delegate
            { _group.cmdJoyStart = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));

        }
        public void ManRelease(GroupLogic _group)
        {
            _group.cmdManStart = true;
            TimedAction.ExecuteWithDelay(new Action(delegate
            { _group.cmdManStart = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));

        }
        public void ManPos_press(GroupLogic _group) { _group.cmdUp = true; }
        public void ManPos_release(GroupLogic _group) { _group.cmdUp = false; }
        public void ManNeg_press(GroupLogic _group) { _group.cmdDown = true; }
        public void ManNeg_release(GroupLogic _group) { _group.cmdDown = false; }
        public void ResetTrip(GroupLogic _group)
        {
            _group.cmdRstTrip = true;
            TimedAction.ExecuteWithDelay(new Action(delegate
            { _group.cmdRstTrip = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));
            //foreach (MotorLogic motor in _group.MotorsInGroup.Where(q => q.Trip == true))
            //{
            //    UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 0, SdoPriority.hi);
            //    Message += string.Format(" Motor={0}, MotID={1}, CP={2};", motor.Title, motor.MotorID, motor.CP);
            //}
            //if (_group.groupSM.state == states.Error2)
            //{
            //    _group.timeOutTime = DateTime.Now + TimeSpan.FromSeconds(1);
            //    _group.groupSM.state = states.Error3;
            //}
            Log.Write("(GROUP resetTrip)." + Message, EventLogEntryType.Information);
            Message = "";
        }

        public void ResetGroup(GroupLogic _group)
        {
            string s = "";
            try
            {
                for (int i = _group.MotorsInGroup.Count - 1; i >= 0; i--)
                {
                    s += string.Format(" Motor={0}, MotID={1}, CP={2};", _group.MotorsInGroup[i].Title,
                        _group.MotorsInGroup[i].MotorID, _group.MotorsInGroup[i].CP);
                    _group.MotorsInGroup[i].GroupNo = -1;
                }
                //if (_group.groupNo == 0) foreach (var hoist in Sys.UpDownList) hoist.Group = false;
                Log.Write(string.Format("(ResetGroup GroupGUI). " + s), EventLogEntryType.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using LogAlertHB;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Media;
using System.Diagnostics;

namespace StClient
{

    public class Commands
    {
        static string Message;

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        //Point rootPoint;
        //double coef = 1;
        double tempMV;
        double Tollerance = Properties.Settings.Default.PositionTollerance; //ovde

        //public void GrpAuto(GroupLogic _grp)
        //{
        //    if (_grp.MotorsInGroup.Count == 0) return;

        //    if (_grp.MotorsInGroup.All(mot => mot.state == states.Idle && mot.autoEnabled == true))
        //    {
        //        Message = "";
        //        foreach (var mot in _grp.MotorsInGroup)
        //        {
        //            mot.TP = _grp.SP;
        //            mot.TV = _grp.SV;
        //            if (Math.Abs(mot.TP - mot.CP) > Tollerance)
        //            {
        //                SoloAuto(mot);
        //                Message += string.Format("\n Motor={0}, ID={1}, SP={2} SV={3} CP={4};",
        //                    mot.Title, mot.MotorID, mot.TP, mot.TV, mot.CP);
        //            }
        //        }
        //        if (Message != "") Log.Write("SM(GrpAuto). " + Message, EventLogEntryType.Information);

        //    }
        //}

        //public void CueStarting(List<MotorLogic> Motors)
        //public void CueStart(CueItem cueItem)
        //{
        //    MotorLogic mot = cueItem.Motor;
        //    Message = "";
        //        mot.TP = cueItem.SP;
        //        mot.TV = cueItem.SV;
        //        if ((Math.Abs(mot.TP - mot.CP) > Properties.Settings.Default.PositionTollerance) &&
        //            mot.soloSM.state == states.Idle)
        //        {
        //            mot.soloSM.state = states.AutoStarting1;
        //            Message += string.Format("\n Motor={0}, ID={1}, SP={2} SV={3} CP={4};",
        //                mot.Title, mot.MotorID, mot.TP, mot.TV, mot.CP);
        //        }
        //        Log.Write("SM(CueStart). " + Message, EventLogEntryType.Information);
        //    if (Message != "") Log.Write("SM(CueStart). " + Message, EventLogEntryType.Information);
        //}

        //public void AutoStarting(List<MotorLogic> Motors)
        ////public void AutoStarting(List<MotorLogic> Motors, object Iclass)
        ////public void AutoStarting(List<MotorLogic> Motors, ISpSv _iSpSv, ISyncIMotorsList _iCalcEnabled)
        //{
        //    if (Motors.Count == 0) return;

        //    foreach (MotorLogic motor in Motors)
        //    {
        //        //Sys.printStack(motor);
        //        //motor.iSpSv = _iSpSv;
        //        //motor.calculatesEnabledType = _iCalcEnabled;

        //        //motor.iSpSv = (ISpSv)Iclass;
        //        //if (Iclass.GetType() == typeof(CueItem)) //stimung 
        //        //    motor.calculatesEnabledType = (ISyncIMotorsList)CueLogic.UniqueInstance;
        //        //else
        //        //{
        //        //    motor.calculatesEnabledType = (ISyncIMotorsList)Iclass;
        //        //    //motor.iMv = (IMv)Iclass; //todo todo 20110221 ja ga skinuo (kakve veze ima mv sa cue) pa ga onda i vratio
        //        //    //motor.calculatesEnabledType
        //        //}


        //        if (Math.Abs(motor.SP - motor.CP) > Tollerance && Motors.All(qq => qq.autoEnabled))
        //        {
        //            motor.SbcLock = MotorBrake.Unlocked;
        //            //coef = motor.calculatesEnabledType.IsSync ? motor.calculatesEnabledType.SyncValue / motor.MaxV : 1;
        //            UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.SpIndex, CanSett.SpSubIndex, motor.SP, SdoPriority.hi);
        //            UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.SvIndex, CanSett.SvSubIndex, motor.SV, SdoPriority.hi);
        //            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandAuto, CanSett.SdoProcTime * 2, 2);
        //            //Debug.WriteLine(string.Format("AUTO: {2}, MOTOR: {0}, SP:{1}, STATE: {3}, BRZINA: {4}", motor.MotorID, motor.positionVelocitySetType.SP, motor.GroupNo, motor.state, motor.positionVelocitySetType.SV * coef));
        //            motor.soloSM.state = states.AutoStarting2;
        //        }
        //        else
        //            motor.soloSM.state = states.Idle;

        //        //motor.autoStartedTimeStamp = DateTime.Now;
        //        Message += string.Format(" Motor={0}, ID={1}, SP={2} SV={3} CP={4};", motor.Title, motor.MotorID, motor.SP, motor.SV, motor.CP);
        //        //Sys.printStack(motor);
        //    }
        //    if (Message != "")
        //    {
        //        Log.Write("SM(AutoStarting). " + Message, EventLogEntryType.Information);
        //        Message = "";

        //    }
        //}

        //public void Stop(List<MotorLogic> motorsList)
        //{
        //    if (motorsList.Count > 0 && motorsList.All(qq => qq.stopEnabled))
        //    {
        //        foreach (MotorLogic motor in motorsList)
        //        {
        //            Stopping(motor);
        //            Message += string.Format(" Motor={0}, MotID={1}, CP={2}, CV={3},", motor.Title, motor.MotorID, motor.CP, motor.CV);
        //        }


        //        if (Message != "")
        //        {
        //            Log.Write("SM(Stopping)." + Message, EventLogEntryType.Information);
        //            Message = "";
        //        }
        //    }
        //}

        //public void JoyStart(List<MotorLogic> motorsList)/
        //public void JoyStart(GroupLogic _group)
        //{
        //    if (_group.MotorsInGroup.Count == 0 || _group.MotorsInGroup.Any(qq => qq.iMotorIndicators.manEnabled == false)) return;
        //    if (!_group.MotorsInGroup.All(qq => qq.soloSM.state == states.Idle) ||
        //        _group.groupSM.state != states.Idle) return;

        //    _group.groupSM.state = states.JoyStarting;
        //}
        //public void JoyEnd(GroupLogic _group)
        //{
        //    if (_group.MotorsInGroup.Count == 0 || _group.Brake == true) return;
        //    if (_group.groupSM.state == states.JoyStarted) _group.groupSM.state = states.JoyEnded;
        //}
        //public void JoyButtonPressed(GroupLogic _group)
        //{
        //    if (_group.MotorsInGroup.Count == 0 || _group.MotorsInGroup.Any(qq => qq.iMotorIndicators.manEnabled == false)) return;

        //    foreach (MotorLogic motor in _group.MotorsInGroup)
        //        if (motor.state == states.JoyStarted) motor.state = states.JoyButtPressed;
        //}
        //public void JoyButtonReleased(GroupLogic _group)
        //{
        //    if (_group.MotorsInGroup.Count == 0 || _group.MotorsInGroup.Any(qq => qq.iMotorIndicators.manEnabled == false)) return;

        //    foreach (MotorLogic motor in _group.MotorsInGroup)
        //        if (motor.state == states.JoyButtPressed || motor.state == states.JoyPos || motor.state == states.JoyNeg)
        //            motor.state = states.JoyButtReleased;
        //}


        //public void ManualStarting(List<MotorLogic> motorsList, object Iclass)
        //public void ManualStarting(List<MotorLogic> motorsList)//for solo or group (only man, not joy)
        //{
        //    if (motorsList.Count == 0 || motorsList.Any(qq => qq.iMotorIndicators.manEnabled == false)) return;

        //    foreach (MotorLogic motor in motorsList)
        //    {
        //        //motor.calculatesEnabledType = (ISyncIMotorsList)Iclass;
        //        motor.iMotorIndicators.ManTimeOut = DateTime.Now + TimeSpan.FromSeconds(5);
        //        //coef = motor.calculatesEnabledType.IsSync ? motor.calculatesEnabledType.SyncValue / motor.MaxV : 1;
        //        //tempMV = coef * Math.Abs(motor.iMv.MV);
        //        tempMV = Math.Abs(motor.MV);
        //        UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.MvIndex, CanSett.MvSubIndex, tempMV, SdoPriority.hi);
        //        UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.Direction, 0, 5);
        //        motor.soloSM.state = states.ManStarted2;
        //        //Debug.WriteLine(string.Format("GRUPA: {2}, MOTOR: {0}, PRAVAC:{1}, SYNC: {3}", motor.MotorID, motor.manualVelocityType.Direction, motor.GroupNo, motor.calculatesEnabledType.IsSync));
        //        // Debug.WriteLine(string.Format("manstarting    GRUPA: {2}, MOTOR: {0}, PRAVAC:{1}, manv: {3}", motor.MotorID, motor.iMv.Direction, motor.GroupNo, tempMV));

        //        Message += string.Format(" Motor={0}, MotID={1}, CP={2};", motor.Title, motor.MotorID, motor.CP);
        //    }

        //    if (Message != "")
        //    {
        //        Log.Write("SM(ManualStarting)." + Message, EventLogEntryType.Information);
        //        Message = "";
        //    }
        //}

        //public void Up_PreviewMouseLeftButtonDown(List<MotorLogic> motorsList)
        //{
        //    if (!(motorsList.Count > 0 && motorsList.All(qq => qq.iMotorIndicators.manPosNegEnabled))) return;

        //    foreach (MotorLogic motor in motorsList)
        //    {
        //        if (motor.soloSM.state == states.ManStarted2) motor.soloSM.state = states.ManPos;
        //        //UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.PosDirection, 0, 2);
        //        //motor.state = states.ManStarted2;
        //        Message += string.Format(" Motor={0}, MotID={1}, CP={2}, CV={3};", motor.Title, motor.MotorID, motor.CP, motor.CV);
        //    }
        //    if (Message != "")
        //    {
        //        Log.Write("(Up_PreviewMouseLeftButtonDown)." + Message, EventLogEntryType.Information);
        //        Message = "";
        //    }
        //}

        //public void UpDown_PreviewMouseLeftButtonUp(List<MotorLogic> motorsList)
        //{
        //    foreach (MotorLogic motor in motorsList)
        //    {
        //        if (motor.soloSM.state == states.ManNeg || motor.soloSM.state == states.ManPos)
        //            motor.soloSM.state = states.ManButtReleased;
        //        Message += string.Format(" Motor={0}, MotID={1}, CP={2}, CV={3};", motor.Title, motor.MotorID, motor.CP, motor.CV);
        //    }
        //    if (Message != "")
        //    {
        //        Log.Write("(UpDown_PreviewMouseLeftButtonUp)." + Message, EventLogEntryType.Information);
        //        Message = "";
        //    }
        //}

        //public void Down_PreviewMouseLeftButtonDown(List<MotorLogic> motorsList)
        //{
        //    if (!(motorsList.Count > 0 && motorsList.All(qq => qq.iMotorIndicators.manPosNegEnabled))) return;

        //    foreach (MotorLogic motor in motorsList)
        //    {
        //        if (motor.soloSM.state == states.ManStarted2) motor.soloSM.state = states.ManPos;
        //        //UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.NegDirection, 0, 2);
        //        //motor.state = states.ManStarted2;
        //        Message += string.Format(" Motor={0}, MotID={1}, CP={2}, CV={3};", motor.Title, motor.MotorID, motor.CP, motor.CV);
        //    }

        //    if (Message != "")
        //    {
        //        Log.Write("(Down_PreviewMouseLeftButtonDown)." + Message, EventLogEntryType.Information);
        //        Message = "";
        //    }
        //}

        ////}


        //public void resetTrip(List<MotorLogic> motorsList)
        //{
        //    foreach (MotorLogic motor in motorsList)
        //    {
        //        UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 0, SdoPriority.hi);
        //        Message += string.Format(" Motor={0}, MotID={1}, CP={2};", motor.Title, motor.MotorID, motor.CP);
        //    }
        //    if (Message != "")
        //    {
        //        Log.Write("(resetTrip)." + Message, EventLogEntryType.Information);
        //        Message = "";
        //    }
        //}

        //public void removeLimits(MotorLogic motor)
        //{
        //    UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.LimPosIndex, CanSett.LimSubIndex, motor.LimPos + 3000, SdoPriority.hi);
        //    Log.Write(string.Format("(Remove Limits). limpos moved to ={0}, Motor={1}, MotID={2}, CP={3}", motor.LimPos + 3000, motor.Title, motor.MotorID, motor.CP), EventLogEntryType.Information);

        //    UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.LimNegIndex, CanSett.LimSubIndex, motor.LimNeg - 3000, SdoPriority.hi);
        //    Log.Write(string.Format("(Remove Limits). limneg moved to ={0}, Motor={1}, MotID={2}, CP={3} ", motor.LimNeg - 3000, motor.Title, motor.MotorID, motor.CP), EventLogEntryType.Information);

        //    motor.LimNegFromServo = motor.LimPosFromServo = double.NaN;
        //}

        //public void setLimits(MotorLogic motor)
        //{
        //    UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.LimPosIndex, CanSett.LimSubIndex, motor.LimPos, SdoPriority.hi);
        //    Log.Write(string.Format("(Set Limits to normal). limpos set to ={0}, Motor={1}, MotID={2}, CP={3}", motor.LimPos, motor.Title, motor.MotorID, motor.CP), EventLogEntryType.Information);

        //    UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.LimNegIndex, CanSett.LimSubIndex, motor.LimNeg, SdoPriority.hi);
        //    Log.Write(string.Format("(Set Limits to normal). limneg set to ={0}, Motor={1}, MotID={2}, CP={3}", motor.LimNeg, motor.Title, motor.MotorID, motor.CP), EventLogEntryType.Information);

        //    motor.LimNegFromServo = motor.LimPosFromServo = double.NaN;
        //}

        //public void ReadLimitsFromServo(MotorLogic motor)
        //{
        //    UdpTx.UniqueInstance.SendSDOReadRQ(motor.MotorID, CanSett.LimPosIndex, CanSett.LimSubIndex, SdoPriority.lo);
        //    UdpTx.UniqueInstance.SendSDOReadRQ(motor.MotorID, CanSett.LimNegIndex, CanSett.LimSubIndex, SdoPriority.lo);
        //}




        public void ProcessKeyInput(string key)
        {
            if (Sys.receivedTxtBlock != null)
            {
                Sys.receivedTxtBlock.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
                            {
                                TextComposition textComposition = new TextComposition(InputManager.Current, Sys.receivedTxtBlock, key);
                                TextCompositionEventArgs e = new TextCompositionEventArgs(Keyboard.PrimaryDevice, textComposition);
                                e.RoutedEvent = UIElement.TextInputEvent;
                                Sys.receivedTxtBlock.RaiseEvent(e);
                                Sys.receivedTxtBlock.Focus();

                            });
            }
        }

        public void ProcessKeyInput(Key key)
        {
            if (Sys.receivedTxtBlock != null)
            {
                Sys.receivedTxtBlock.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
                           {
                               KeyEventArgs args = new KeyEventArgs(Keyboard.PrimaryDevice, PresentationSource.FromVisual(Sys.receivedTxtBlock), 0, key);
                               args.RoutedEvent = UIElement.KeyDownEvent;
                               Sys.receivedTxtBlock.RaiseEvent(args);
                               Sys.receivedTxtBlock.Focus();
                           });
            }
        }

        //public void Stopping(MotorLogic motor)
        //{
        //    motor.soloSM.state = states.Stopping1;
        //}

        public void StopAllMotors()//todo20131228 kandidat za brisanje. 20140413 nkk all stop?????
        {
            foreach (MotorLogic motor in Sys.StaticMotorList.Where(m => m.Brake == false))
                motor.soloSM.stopMotor();
            //foreach (MotorLogic motor in Sys.StaticMotorList.Where(m => m.Brake == false))
            //    Stopping(motor);

            //KompenzacioneRxTx.TotalStop();

            //Sys.vagoniRxTx.TotalStop();

            //Sys.vagoniRxTx.rotStop();

        }

        public void Send_InitAll()
        {
            UdpTx.UniqueInstance.SendPDO_InitAll();
        }







    }
}
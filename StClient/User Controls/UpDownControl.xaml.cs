﻿using LogAlertHB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Serialization;

namespace StClient
{
    /// <summary>
    /// Interaction logic for UpDownControl.xaml
    /// </summary>
    public partial class UpDownControl : UserControl
    {
        public UpDownControl(UpDownLogic udl)
        {
            InitializeComponent();

            this.DataContext = t = udl;
        }
        UpDownLogic t;
        private void _up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UdpTx.UniqueInstance.Send8200PDO(Up: true, Down: false, CanAdr: t.canAdr);
            Log.Write(string.Format("(UP pressed HoistGUI). {0}", hoistInfo()), EventLogEntryType.Information);
        }

        private void _PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            UdpTx.UniqueInstance.Send8200PDO(Up: false, Down: false, CanAdr: t.canAdr);
            Log.Write(string.Format("(UP/DOWN released HoistGUI). {0}", hoistInfo()), EventLogEntryType.Information);
        }

        private void _down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UdpTx.UniqueInstance.Send8200PDO(Up: false, Down: true, CanAdr: t.canAdr);
            Log.Write(string.Format("(DOWN pressed HoistGUI). {0}", hoistInfo()), EventLogEntryType.Information);
        }
        string hoistInfo() { return string.Format("HOIST: name={0} can={1},", t.Title, t.canAdr); }

        private void InGroup_Checked(object sender, RoutedEventArgs e)
        {
            //Sys.StaticGroupList[0].ListviewMotorCollection.Add(new LVMotorsInGroup { Title = t.Title });
        }

        private void InGroup_Unchecked(object sender, RoutedEventArgs e)
        {
            //LVMotorsInGroup tempHoist = Sys.StaticGroupList[0].ListviewMotorCollection.First(qq => qq.Title == t.Title);//try catch here? throws exceptions
            //if (tempHoist == null) return;
            //Sys.StaticGroupList[0].ListviewMotorCollection.Remove(tempHoist);

        }
    }

    public class UpDownLogic : INotifyPropertyChanged
    {

        int _motorID = 301, _row = 0, _column = 0, _canAdr = 3;
        string _title = "simpleHoist";
        bool _group;
        //bool _up, _down, _group;

        //       [XmlAttribute("UP")]
        [XmlIgnore]
        public bool Group { get { return _group; } set { _group = value; OnNotify("Group"); } }
        //[XmlIgnore]
        //public bool Up { get { return _up; } set { _up = value; } }
        //[XmlIgnore]
        //public bool Down { get { return _down; } set { _down = value; } }
        [XmlAttribute("Title")]
        public string Title { get { return _title; } set { if (_title != value) { _title = value; OnNotify("Title"); } } }
        [XmlAttribute("MotorID")]
        public int MotorID { get { return _motorID; } set { _motorID = value; } }
        [XmlAttribute("ROW")]
        public int Row { get { return _row; } set { _row = value; } }
        [XmlAttribute("COLUMN")]
        public int Column { get { return _column; } set { _column = value; } }
        [XmlAttribute("canAdr")]
        public int canAdr { get { return _canAdr; } set { _canAdr = value; } }


        public UpDownLogic() { }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        #endregion
    }

    //public class UpDownSystem
    //{
    //    //static DispatcherTimer _timer = new DispatcherTimer();
    //    //static DateTime timeout;

    //    //public void setAll() { foreach (var t in Sys.UpDownList) t.Group = true; }
    //    //public void setNone() { foreach (var t in Sys.UpDownList) t.Group = false; }

    //    public void groupUp_press()
    //    {
    //        foreach (var t in Sys.UpDownList.Where(q => q.Group == true))
    //            UdpTx.UniqueInstance.Send8200PDO(Up: true, Down: false, CanAdr: t.canAdr);
    //        //t.Up = true;
    //        //startTimer();
    //    }
    //    public void groupDown_press()
    //    {
    //        foreach (var t in Sys.UpDownList.Where(q => q.Group == true))
    //            UdpTx.UniqueInstance.Send8200PDO(Up: false, Down: true, CanAdr: t.canAdr);
    //        //    t.Down = true;
    //        //startTimer();
    //    }
    //    public void groupUpDown_release()
    //    {
    //        foreach (var t in Sys.UpDownList)
    //            UdpTx.UniqueInstance.Send8200PDO(Up: false, Down: false, CanAdr: t.canAdr);
    //        //t.Up = t.Down = false;
    //    }

    //    public UpDownSystem()
    //    {
    //        //_timer.Interval = TimeSpan.FromMilliseconds(200);
    //        //_timer.Tick += _timer_Tick;
    //    }

    //    //private void _timer_Tick(object sender, EventArgs e)
    //    //{
    //    //    if (DateTime.Now > timeout) _timer.Stop();
    //    //    //send pdo
    //    //    foreach (var t in Sys.UpDownList.Where(q => q.Group == true)) UdpTx.UniqueInstance.Send8200PDO(t.Up, t.Down, t.canAdr);
    //    //}

    //    //void startTimer()
    //    //{
    //    //    timeout = DateTime.Now + TimeSpan.FromMinutes(5);
    //    //    _timer.Start();
    //    //}

    //    //void StopTimer() { timeout = DateTime.Now + TimeSpan.FromSeconds(2); }
    //}
}

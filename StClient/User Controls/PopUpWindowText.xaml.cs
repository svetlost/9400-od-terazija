﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace StClient
{
    /// <summary>
    /// Interaction logic for PopUpWindowText.xaml
    /// </summary>
    public partial class PopUpWindowText : Window
    {
        public PopUpWindowText(double X, double Y, string title, string msgTxt)
        {
            InitializeComponent();

            tbTitle.Text = title;
            tbText.Text = msgTxt;
            this.Left = X;
            this.Top = Y;

            this.ShowDialog();
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            this.Topmost = false;
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            this.Topmost = true;
        }

    }



}

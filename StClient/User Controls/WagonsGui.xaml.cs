﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StClient
{
    /// <summary>
    /// Interaction logic for WagonsGui.xaml
    /// </summary>
    public partial class WagonsGui : UserControl
    {
        public WagonsGui()
        {
            InitializeComponent();

            Wag1_L1.DataContext = Sys.vagoniRxTx.wL1;
            Wag2_L2.DataContext = Sys.vagoniRxTx.wL2;
            Wag3_L3.DataContext = Sys.vagoniRxTx.wL3;
            Wag4_R1.DataContext = Sys.vagoniRxTx.wR1;
            Wag5_R2.DataContext = Sys.vagoniRxTx.wR2;
            Wag6_R3.DataContext = Sys.vagoniRxTx.wR3;
            Wag7_ROT.DataContext = Sys.vagoniRxTx.wROT;

            Wag7_ROT.Grp.Visibility = Visibility.Hidden;

            GoGui_WagSide.DataContext = Sys.vagoniRxTx;
            GoGui_WagRot.DataContext = Sys.vagoniRxTx;

            //BindingOperations.SetBinding(g, grph000.CPProperty, new Binding("CP") { Source = motor });

            //GoGui_WagSide.DataContext = VagoniRxTx.UniqueInstance;
            //BindingOperations.SetBinding(  GoGui_WagSide,VagonGoGui.


            //Komp1_L1.DataContext = KompenzacioneRxTx.UniqueInstance.KP_L1;
            //Komp2_L2.DataContext = KompenzacioneRxTx.UniqueInstance.KP_L2;
            //Komp3_L3.DataContext = KompenzacioneRxTx.UniqueInstance.KP_L3;
            //Komp4_R1.DataContext = KompenzacioneRxTx.UniqueInstance.KP_R1;
            //Komp5_R2.DataContext = KompenzacioneRxTx.UniqueInstance.KP_R2;
            //Komp6_R3.DataContext = KompenzacioneRxTx.UniqueInstance.KP_R3;
            //Komp7_ROT.DataContext = KompenzacioneRxTx.UniqueInstance.KP_ROT;

            //Komp1_L1.komp = KompenzacioneRxTx.UniqueInstance.KP_L1;
            //Komp2_L2.DataContext = KompenzacioneRxTx.UniqueInstance.KP_L2;
            //Komp3_L3.DataContext = KompenzacioneRxTx.UniqueInstance.KP_L3;
            //Komp4_R1.DataContext = KompenzacioneRxTx.UniqueInstance.KP_R1;
            //Komp5_R2.DataContext = KompenzacioneRxTx.UniqueInstance.KP_R2;
            //Komp6_R3.DataContext = KompenzacioneRxTx.UniqueInstance.KP_R3;
            //Komp7_ROT.DataContext = KompenzacioneRxTx.UniqueInstance.KP_ROT;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StClient
{
   public class StartecGroupLogic
    {
        public List<StartecLogic> StartecsInGroup = new List<StartecLogic>();
        public StartecGroupLogic()
        {

        }

        public void AddToGroup(StartecLogic item)
        {
            StartecsInGroup.Add(item);
        }

        public void RemoveFromGroup(StartecLogic item)
        {
            StartecsInGroup.Remove(item);
           
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StClient
{
    /// <summary>
    /// Interaction logic for StartecGroup.xaml
    /// </summary>
    public partial class StartecGroup : UserControl
    {
        StartecGroupLogic stgl;

        public StartecGroup(StartecGroupLogic stgl)
        {
            InitializeComponent();
            this.stgl = stgl;
        }

        private void RemoveGroup_Click(object sender, RoutedEventArgs e)
        {

            List<StartecLogic> stl = Sys.StaticStartecList.FindAll(qq => qq.isCHKchecked);
            if (stl != null) stl.ForEach(qq => qq.isCHKchecked = false);

        }

        private void Up_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

            UdpTx.UniqueInstance.SendHoistPDO(0);

        }

        private void Up_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            long i = 0;
            foreach (StartecLogic stl in stgl.StartecsInGroup)
            {
                i = i + stl.Up;
            }

            UdpTx.UniqueInstance.SendHoistPDO(i);
        }

        private void Down_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            long i = 0;
            foreach (StartecLogic stl in stgl.StartecsInGroup)
            {
                i = i + stl.Down;
            }
            UdpTx.UniqueInstance.SendHoistPDO(i);
        }

        private void Down_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

            UdpTx.UniqueInstance.SendHoistPDO(0);

        }
    }
}

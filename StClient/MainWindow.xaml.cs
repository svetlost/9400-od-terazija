﻿using LogAlertHB;
using Microsoft.Win32;
using Phidgets;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;  //needed for the event handling classes
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Threading;

using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;


namespace StClient
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class MainWindow : Window
    {



        public Sys sys;
        //CREATE SENDER RECEIVER
        UdpRx _UdpRx;
        UdpTx _UdpTx;


        VisibilityStatesLogic mvstates = new VisibilityStatesLogic();
        ListBox lb;
        public static Dispatcher mainWindowDispacher;
        // public static int XXX;
        ApplicationStateLogic mainWindowEnabled = ApplicationStateLogic.Instance;
        DispatcherTimer MainLoop = new DispatcherTimer();

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        UpDownControl ZavServis;

        public MainWindow()
        {
            InitializeComponent();

            this.DataContext = mainWindowEnabled;
            mainWindowDispacher = this.Dispatcher;

            Microsoft.Win32.SystemEvents.PowerModeChanged += OnPowerChange;

            sys = new Sys();

            //CREATE GROUPS
            foreach (GroupLogic gl in Sys.StaticGroupList)
            {
                GroupGUI gGUI = new GroupGUI(gl);
                lbOfGroups.Items.Add(gGUI);
            }

            //CREATE DRIVES
            lbOfMotors.Items.Clear();
            lbOfCuesMotorGUI.Items.Clear();
            foreach (MotorLogic motor in Sys.StaticMotorList.FindAll(t => t.Enabled == true))
            {
                lbOfMotors.Items.Add(new MotorGUI(motor));
                lbOfCuesMotorGUI.Items.Add(new CuesMotorGUI(motor));
                DiagLeft.Children.Add(new DiagGUI(motor));
                GraphLeft.Children.Add(new Graphic(motor));

                ////TC only, TC=can17..can24, cluster=3
                //if (motor.ClusterLogical==3)
                //    MiscLeft.Children.Add(new TC(motor));



                // CueGraphLeft.Children.Add(new GraphGui2(motor));
            }
            //MiscLeft.Children.Add(new Test2());
            CueListBox.Children.Add(CueListView.Instance);

            //UdpBcastTst ut = new UdpBcastTst();
            //MiscLeft.Children.Add(new UdpBcastTst());
            //vagonGrid.Children.Add(new UdpBcastTst());//20140119
            //todo dodato 2013 zvagone


            //HOIST
            foreach (UpDownLogic ud in Sys.UpDownList)
            {
                UpDownControl tmp = new UpDownControl(udl: ud);
                _hoistsGrid.Children.Add(tmp);
                Grid.SetColumn(tmp, ud.Column);
                Grid.SetRow(tmp, ud.Row);

                if (ud.MotorID == 321)
                {
                    ZavServis = tmp;
                    ZavServis.Visibility = Visibility.Hidden;
                }
            }
            //foreach (StartecLogic starteclogic in Sys.StaticStartecList)
            //{
            //    Startec startec = new Startec(starteclogic);
            //    RcAndHoist.Children.Add(startec);
            //    Grid.SetColumn(startec, starteclogic.Column);
            //    Grid.SetRow(startec, starteclogic.Row);
            //}


            //listBoxes
            Sys.MotorListbox = lbOfMotors;
            Sys.GroupListBox = lbOfGroups;
            Sys.CuesMotorGUIListBox = lbOfCuesMotorGUI;



            _UdpRx = new UdpRx();
            _UdpTx = UdpTx.UniqueInstance;
            string s1 = ((_UdpRx as IInitResult).initResult) + "\n" + ((_UdpTx as IInitResult).initResult);
            Log.Write(s1, EventLogEntryType.Information);
            AlertLogic.Add(s1);

            //CLUSTER ELECTRIC
            DiagRight.Children.Add(SumsPPsPanel.Instance);
            // if (Sys.render3D) d3.Children.Add(new view3d()); //20140211 cekaju se bolja vremena da se stavi 3d model todo
            //Tab3D.Visibility = Visibility.Collapsed;
            //TabVagon.Visibility = Visibility.Collapsed;

            //ALERT  LIST VIEW
            //AlertsListView alview = new AlertsListView();
            //Grid3dLog.Children.Add(alview);
            //Grid.SetColumn(alview, 1);


            //AlertsListView alview1 = new AlertsListView();
            //CuesRight.Children.Add(alview1);
            //Grid.SetRow(alview1, 2);

            AlertsListView alview2 = new AlertsListView();
            ttt.Children.Add(alview2);
            //Grid.SetRow(alview2, 1);

            ////diag2
            //todo cleanup
            Signal sig1 = new Signal() { text = "LP1", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig1, Signal.valueProperty, new Binding("LP1") { Source = ApplicationStateLogic.Instance });
            Signal sig2 = new Signal() { text = "LP2", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig2, Signal.valueProperty, new Binding("LP2") { Source = ApplicationStateLogic.Instance });
            Signal sig3 = new Signal() { text = "ESTOP", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig3, Signal.valueProperty, new Binding("Estop_Pressed") { Source = ApplicationStateLogic.Instance });
            Signal sig4 = new Signal() { text = "PULT_PK1", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig1, Signal.valueProperty, new Binding("PULT_PK1") { Source = ApplicationStateLogic.Instance });
            Signal sig5 = new Signal() { text = "PULT_PK2", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig1, Signal.valueProperty, new Binding("PULT_PK2") { Source = ApplicationStateLogic.Instance });
            Signal sig6 = new Signal() { text = "PCP_PK1", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig1, Signal.valueProperty, new Binding("PCP_PK1") { Source = ApplicationStateLogic.Instance });
            Signal sig7 = new Signal() { text = "PCP_PK2", Width = 105, VerticalAlignment = VerticalAlignment.Center, onColor = Brushes.Red, offColor = Brushes.Black, Margin = new Thickness(2) };
            BindingOperations.SetBinding(sig1, Signal.valueProperty, new Binding("PCP_PK2") { Source = ApplicationStateLogic.Instance });
            //Diag2Left.Children.Add(sig1);
            wrapSysDiag.Children.Add(sig3);
            //Diag2Left.Children.Add(sig2);
            wrapSysDiag.Children.Add(sig7);
            wrapSysDiag.Children.Add(sig6);
            wrapSysDiag.Children.Add(sig5);
            wrapSysDiag.Children.Add(sig4);           
            wrapSysDiag.Children.Add(sig2);
            wrapSysDiag.Children.Add(sig1);
            wrapSysDiag.Children.Add(new TextBlock() { Text = "xxx", Foreground = Brushes.Transparent });
          //  wrapSysDiag.Children.Add(new GelbauGUI());

            //AlertsListView alview4 = new AlertsListView();
            //MiscRightGrid.Children.Add(alview4);
            //Grid.SetRow(alview4, 1);


            //StartecGroup stl = new StartecGroup(Sys.StartecGroupLogic);
            //RCHoistRight.Children.Add(stl);
            //Grid.SetRow(stl, 0);

            //AlertsListView alview3 = new AlertsListView();
            //AlertsListView alview3 = new AlertsListView(850);
            //alview3.ve
            //RCHoistRight2.Children.Add(alview3);
            //Grid.SetRow(alview3, 1);

            Sys.TabControl = MainWindowTabControl;
            PhidgetsInterfaces.UniqueInstance.initPhidget();

            MainLoop.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval);
            MainLoop.Tick += new EventHandler(MainLoop_Tick);
            MainLoop.Start();

            TimedAction.ExecuteWithDelay(new Action(delegate
            {
                foreach (MotorLogic motor in Sys.StaticMotorList.FindAll(t => t.Can)) motor.SP = motor.CP;
            }), TimeSpan.FromSeconds(4));

            SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(mainloopTimeCounter), 5);
        }
        CounterSumsLogic mainloopTimeCounter = new CounterSumsLogic() { Names = "mainloopTime us" };
        //CounterSumsLogic rXtotal = new CounterSumsLogic()
        DateTime loopStart;

        void MainLoop_Tick(object sender, EventArgs e)
        {
            loopStart = DateTime.Now;
            CueLogic.UniqueInstance.UpdateCurrentCueItemsList();

            CueLogic.UniqueInstance.CalculateEnableds();
            VisibilityStatesLogic.UniqueInstance.CalculateSystemIndicators();

            foreach (GroupLogic gl in Sys.StaticGroupList)
            {
                gl.CalculateEnableds();
                gl.groupSM.ProcessGroupStateMachine();
            }

            foreach (MotorLogic ml in Sys.StaticMotorList)
            {

                //ml.iMotorIndicators.CalculateEnableds();//20140526 skinuo i stavio 
                ml.CalculateEnableds();
                ml.soloSM.ProcessStateMachine();

                if (ml.GroupNo == -1)
                    ml.grpResetEnabled = false;
                else
                {
                    GroupLogic gl = Sys.StaticGroupList.Find(g => g.groupNo == ml.GroupNo);
                    ml.grpResetEnabled = (!CueLogic.UniqueInstance.Moving && !gl.Moving);
                }
            }

            //TimeSpan ewr = loopStart - DateTime.Now;
            //long dt = TimeSpan.FromTicks(loopStart.
            //mainloopTimeCounter.Count = ewr.Ticks / TimeSpan.TicksPerMillisecond;
            //mainloopTimeCounter.Count = ewr.Milliseconds;
            mainloopTimeCounter.Count = Convert.ToInt32((DateTime.Now - loopStart).Ticks) / 10;//1s=10 000 000ticks, 1us=10tick
            //CmdMan._process();//not used
        }

        public void ShowTab(int index)
        {
            try { MainWindowTabControl.SelectedIndex = index; }
            catch { }
        }

        private void OnPowerChange(object s, PowerModeChangedEventArgs e)
        {
            if (e.Mode == PowerModes.Suspend) App.Current.Shutdown();
            if (e.Mode == PowerModes.Resume) App.Current.Shutdown();
            //switch (e.Mode)
            //{
            //    case PowerModes.Resume: break;
            //    case PowerModes.Suspend: break;
            //}
        }

        public void Window1_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Control) > 0)
            {
                switch (e.Key)
                {
                    case (Key.F1):
                        ShowTab(0);
                        //PhidgetsInterfaces.UniqueInstance.PanelOutputViews(0);
                        //PhidgetsInterfaces.UniqueInstance.SetActivePanel(PhidgetsInterfaces.PanelTabs.Motors);
                        break;
                    case (Key.F2):
                        ShowTab(1);
                        //PhidgetsInterfaces.UniqueInstance.PanelOutputViews(1);
                        //PhidgetsInterfaces.UniqueInstance.SetActivePanel(PhidgetsInterfaces.PanelTabs.Cues);
                        break;
                    case (Key.F3):
                        ShowTab(2);
                        //PhidgetsInterfaces.UniqueInstance.PanelOutputViews(2);
                        //PhidgetsInterfaces.UniqueInstance.SetActivePanel(PhidgetsInterfaces.PanelTabs.Graph);
                        break;
                    case (Key.F4):
                        ShowTab(3);
                        //PhidgetsInterfaces.UniqueInstance.PanelOutputViews(3);
                        //PhidgetsInterfaces.UniqueInstance.SetActivePanel(PhidgetsInterfaces.PanelTabs.Diag);
                        break;
                    case (Key.F5):
                        //MainGraphicTabControl.SelectedIndex = 0;
                        break;
                    case (Key.F6):
                        //MainGraphicTabControl.SelectedIndex = 1;
                        break;
                }
            }
            else if ((Keyboard.Modifiers & ModifierKeys.Shift) > 0)
            {
                switch (e.Key)
                {
                    case (Key.F1):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(1, true);
                        break;
                    case (Key.F2):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(2, true);
                        break;
                    case (Key.F3):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(3, true);
                        break;
                    case (Key.F4):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(4, true);
                        break;
                    case (Key.F5):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(5, true);
                        break;
                    case (Key.F6):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(6, true);
                        break;
                    case (Key.F7):
                        VisibilityStatesLogic.UniqueInstance.ClusterListAddRemove(7, true);
                        break;
                    case (Key.F8):
                        VisibilityStatesLogic.UniqueInstance.ShowAllShowNone(true);
                        break;
                    case (Key.F9):
                        VisibilityStatesLogic.UniqueInstance.ShowAllShowNone(false);
                        break;
                    case (Key.F10):
                        VisibilityStatesLogic.UniqueInstance.ShowCuedMotors();
                        break;
                    case (Key.F11):
                        VisibilityStatesLogic.UniqueInstance.ShowMovingMotors();
                        break;
                    case (Key.F12):
                        VisibilityStatesLogic.UniqueInstance.ShowTrippedMotors();
                        break;
                }
            }
            else
            {
                switch (e.Key)
                {
                    case (Key.F1):
                        if (Sys.StaticGroupList.Count > 0 && Sys.StaticGroupList[0].MotorsInGroup.Count > 0)
                            VisibilityStatesLogic.UniqueInstance.ShowMotor(Sys.StaticGroupList[0].MotorsInGroup);
                        break;
                    case (Key.F2):
                        if (Sys.StaticGroupList.Count > 1 && Sys.StaticGroupList[1].MotorsInGroup.Count > 0)
                            VisibilityStatesLogic.UniqueInstance.ShowMotor(Sys.StaticGroupList[1].MotorsInGroup);
                        break;
                    case (Key.F3):
                        if (Sys.StaticGroupList.Count > 2 && Sys.StaticGroupList[2].MotorsInGroup.Count > 0)
                            VisibilityStatesLogic.UniqueInstance.ShowMotor(Sys.StaticGroupList[2].MotorsInGroup);
                        break;
                    case (Key.F4):
                        if (Sys.StaticGroupList.Count > 3 && Sys.StaticGroupList[3].MotorsInGroup.Count > 0)
                            VisibilityStatesLogic.UniqueInstance.ShowMotor(Sys.StaticGroupList[3].MotorsInGroup);
                        break;
                    case (Key.F5):
                        if (Sys.StaticGroupList.Count > 4 && Sys.StaticGroupList[4].MotorsInGroup.Count > 0)
                            VisibilityStatesLogic.UniqueInstance.ShowMotor(Sys.StaticGroupList[4].MotorsInGroup);
                        break;
                    case (Key.F6):
                        if (Sys.StaticGroupList.Count > 5 && Sys.StaticGroupList[5].MotorsInGroup.Count > 0)
                            VisibilityStatesLogic.UniqueInstance.ShowMotor(Sys.StaticGroupList[5].MotorsInGroup);
                        break;
                    case (Key.F7):
                        VisibilityStatesLogic.UniqueInstance.ShowOnlyElectricClusteredMotors();
                        break;

                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.KeyDown += new System.Windows.Input.KeyEventHandler(Window1_KeyDown);
        }

        protected override void OnClosed(EventArgs e)
        {
           // PhidgetsInterfaces.UniqueInstance.AllOutsOff();
        }

        DispatcherTimer CloseAppTimer = new DispatcherTimer();
        bool wait = false;

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {

            try
            {
                CloseAppTimer.Tick += new EventHandler(CloseApp_Tick);
                CloseAppTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.WaitItervalForMotorsStopOnExit);

                //Sys.vagoniRxTx.StopRotOnExit_Phase1();
                //Sys.vagoniRxTx.StopRotOnExit();

                List<MotorLogic> tempList = Sys.StaticMotorList.FindAll(t => t.Can && !t.Brake);

                //NkkControl.ClearAll();


                if (tempList.Count > 0 && !wait)
                {
                    string _allMotors = "";

                    foreach (MotorLogic m in tempList)
                        _allMotors += string.Format(" mot={0} motID={1} cp={2} cv={3};", m.Title, m.MotorID, m.CP, m.CV);

                    MessageBoxResult result = MessageBox.Show("Motors " + _allMotors + "are still running. Stop motors?", "Stop motors", MessageBoxButton.YesNo);

                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            e.Cancel = true;
                            wait = false;
                            CloseAppTimer.Start();

                            foreach (MotorLogic motor in tempList)
                            {
                                if (motor.Moving)
                                    UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
                                // UdpTx.UniqueInstance.SendPdoSbc((byte)motor.CanChannel, (byte)motor.SecBrkControlAddress, 0);
                                UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, 50, 8);
                            }

                            CloseAppTimer.Start();

                            Log.Write("On application exit user stopped motors: " + _allMotors, EventLogEntryType.Information);
                            break;

                        case MessageBoxResult.No:
                            Log.Write("On application exit user did not stop motors: " + _allMotors, EventLogEntryType.Warning);
                            break;
                    }
                }
                else
                    base.OnClosing(e);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        void CloseApp_Tick(object sender, EventArgs e)
        {
            List<MotorLogic> tempList = Sys.StaticMotorList.FindAll(t => t.Can && !t.Brake);
            if (tempList.Count == 0)
                wait = true;
            if (wait)
            {
                CloseAppTimer.Stop();

                //Sys.vagoniRxTx.StopRotOnExit_Phase2();
                //Sys.vagoniRxTx.closeSocket();

                Application.Current.Shutdown();
            }
        }
        //try-Catch
        private void lbOfMotors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {


                lb = sender as ListBox;

                if (lb.SelectedItem != null)
                {
                    Sys.SelectedMotor = lb.SelectedItem as MotorGUI;
                    Sys.SelectedGroup = null;
                    Sys.SelectedCuesMotorGUIMotor = null;
                    lbOfGroups.SelectedItem = lbOfCuesMotorGUI.SelectedItem = null;


                }



            }
            catch (Exception ex)
            {
                AlertLogic.Add(ex.Message);
            }
        }
        //try-Catch
        private void lbOfGroups_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {

                lb = sender as ListBox;
                if (lb.SelectedItem != null)
                {
                    Sys.SelectedGroup = lb.SelectedItem as GroupGUI;
                    Sys.SelectedMotor = null;
                    Sys.SelectedCuesMotorGUIMotor = null;
                    lbOfMotors.SelectedItem = lbOfCuesMotorGUI.SelectedItem = null;


                }

            }
            catch (Exception ex)
            {
                AlertLogic.Add(ex.Message);
            }
        }

        private void lbOfCuesMotorGUI_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (Sys.StaticMotorList.All(qq => qq.Brake))
                {
                    lb = sender as ListBox;
                    if (lb.SelectedItem != null)
                    {
                        Sys.SelectedGroup = null;
                        Sys.SelectedMotor = null;
                        Sys.SelectedCuesMotorGUIMotor = lb.SelectedItem as CuesMotorGUI;
                        lbOfMotors.SelectedItem = lbOfGroups.SelectedItem = null;


                    }
                }
                else
                    AlertLogic.Add("Selection Change Not Allowed: Brakes Released ");
            }
            catch (Exception ex)
            {
                AlertLogic.Add(ex.Message);
            }
        }

        private void MainWindowTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabControl tbc = sender as TabControl;
            //PhidgetsInterfaces.UniqueInstance.PanelOutputViews(tbc.SelectedIndex);
            try
            {
                //PhidgetsInterfaces.UniqueInstance.SetActivePanel((PhidgetsInterfaces.PanelTabs)tbc.SelectedIndex);
            }
            catch { }
        }

        private void showHideZavesaServis_Click(object sender, RoutedEventArgs e)
        {
            if (ZavServis == null) return;

            if (ZavServis.Visibility == Visibility.Hidden) ZavServis.Visibility = Visibility.Visible;
            else ZavServis.Visibility = Visibility.Hidden;
        }
    }




}

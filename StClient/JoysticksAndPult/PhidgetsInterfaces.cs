﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Phidgets;         //needed for the interfacekit class and the phidget exception class
using Phidgets.Events;
using System.Windows.Threading;
using System.Windows;
using System.Xml.Serialization;
using System.Reflection;
using LogAlertHB;
using System.Windows.Controls;
using System.Windows.Input;
using System.Diagnostics;
using System.Windows.Data;
using System.Threading;
using StClient.Properties;
using System.ComponentModel;



namespace StClient
{
    /// <summary>
    /// Manager is needed to open all Interface kits
    /// it is the only way it works
    /// phidgets GM=Group or Single Motor;Analog = Joystick with 4 more free inputs  
    /// </summary>
    public class PhidgetsInterfaces : INotifyPropertyChanged
    {
        private RFID rfid; //Declare an RFID object
        //List<RfidItem> rfidList;
        Commands commands = new Commands();


        //XmlUtil<StJoystick> JoystickXml = new XmlUtil<StJoystick>();
        //XmlUtil<RfidItem> rfidXml = new XmlUtil<RfidItem>();
        Manager manager = new Manager();
        InterfaceKit tempIfKit;
        //PhidgetItem tempPhidgetItem;
        //StJoystick phJoystickItem;
        List<MotorLogic> motorsList;
        //public List<StJoystick> joystickList;
        List<InterfaceKit> InterfaceKitList = new List<InterfaceKit>();

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;


        //PhidgetItem JoystickPhidget; //da se ne odredjuje svaki put ko je Joystick u senzorima

        ApplicationStateLogic mainWindowEnabled = ApplicationStateLogic.Instance;

        //string _pathConfig = Environment.CurrentDirectory + @"\Config\";
        //public string pathConfig { get { return _pathConfig; } set { if (_pathConfig != value) { _pathConfig = value; } } }
        //public static List<PhidgetItem> ListofPhidgetItems = new List<PhidgetItem>();


        int AnalogUpDownLeftRight = AIO.Default.ph888_3_joy;

        public InterfaceKit Ph888_3_Analog_Cursors;

        MotorGUI mGUI { get { return Sys.SelectedMotor; } }
        GroupGUI gGUI { get { return Sys.SelectedGroup; } }
        CuesMotorGUI cmGUI { get { return Sys.SelectedCuesMotorGUIMotor; } }

        public static int Yaxis { get; set; }

        TextBlock SPtb, SVtb, MVtb;


        #region Attaching

        //Constructor
        PhidgetsInterfaces()
        {
            manager.Attach += new AttachEventHandler(m_Attach);
            manager.open();
        }
        //Flasher flasher = new Flasher();
        //MANAGER 
        void m_Attach(object sender, AttachEventArgs e)
        {
            if (typeof(InterfaceKit) == e.Device.GetType())
            {
                tempIfKit = new InterfaceKit();
                tempIfKit.Attach += new AttachEventHandler(ifKit_Attach);
                tempIfKit.Detach += new DetachEventHandler(ifKit_Detach);
                tempIfKit.Error += new ErrorEventHandler(ifKit_Error);
                tempIfKit.InputChange += new InputChangeEventHandler(ifKit_InputChange);
                tempIfKit.SensorChange += new SensorChangeEventHandler(ifKit_SensorChange);
                tempIfKit.open();
            }

            //bool init=Flasher.Flash(true, Flasher.FlashType.Normal); //todo razresiti ovu glupost darko20101209
            //Flasher
            if (typeof(RFID) == e.Device.GetType())
            {
                rfid = new RFID();
                rfid.Attach += new AttachEventHandler(rfid_Attach);
                rfid.Detach += new DetachEventHandler(rfid_Detach);
                rfid.Error += new ErrorEventHandler(rfid_Error);
                rfid.Tag += new TagEventHandler(rfid_Tag);
                //rfid.Tag += new TagEventHandler(rfid_Tag);
                rfid.TagLost += new TagEventHandler(rfid_TagLost);
                rfid.open();
            }

        }

        //void rfid_Tag(object sender, TagEventArgs e)
        //{
        //    RfidItem rfiditem = rfidList.Find(qq => qq.RfidTag == e.Tag);
        //    if (rfiditem != null)
        //    {

        //        MainWindow.mainWindowDispacher.Invoke
        //            (DispatcherPriority.Normal, (Action)delegate()
        //            {
        //                mainWindowEnabled.isAppEnabled = true;
        //                string s = string.Format("User login - {0}.", rfiditem.Name);
        //                Log.Write(s + string.Format(" rfid={0}", rfiditem.RfidTag));
        //                AlertLogic.Add(s);
        //            });
        //    }
        //    else
        //        MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate() { mainWindowEnabled.isAppEnabled = false; });
        //}
        void rfid_Tag(object sender, TagEventArgs e)
        {
            MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
                     {
                         RfidItem rfiditem = Sys.rfidList.Find(qq => qq.RfidTag == e.Tag);
                         if (rfiditem != null)
                         {
                             mainWindowEnabled.isAppEnabled = true;
                             string s = string.Format("User login - {0}.", rfiditem.Name);
                             Log.Write(s + string.Format(" rfid={0}", rfiditem.RfidTag), EventLogEntryType.Information);
                             AlertLogic.Add(s);

                         }
                         else
                             mainWindowEnabled.isAppEnabled = true;
                     });
        }


        //2013 darko test, remove!!!
        //void rfid_TagTEST(object sender, TagEventArgs e)
        //{
        //    mainWindowEnabled.isAppEnabled = true;
        //}


        void rfid_TagLost(object sender, TagEventArgs e)
        {
            MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate ()
            {
                RfidItem rfiditem = Sys.rfidList.Find(qq => qq.RfidTag == e.Tag);

                if (mainWindowEnabled.isAppEnabled != false)
                {
                    string s = string.Format("User logout - {0}.", rfiditem.Name);
                    mainWindowEnabled.isAppEnabled = true;
                    List<MotorLogic> MovingMotorsList = Sys.StaticMotorList.FindAll(t => t.Can == true && t.Brake == false);
                    if (MovingMotorsList.Count > 0)
                    {
                        s += " Stopping motors.";

                        foreach (MotorLogic motor in MovingMotorsList)
                        {
                            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
                            UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, 50, 8);
                            s += string.Format(" Motor={0}, ID={1}, CP={2}, CV={3};", motor.Title, motor.MotorID, motor.CP, motor.CV);
                        }
                    }
                    Log.Write(s + string.Format(" rfid={0}", rfiditem.RfidTag), EventLogEntryType.Warning);
                    AlertLogic.Add(s);
                }
            });
        }
        //void rfid_TagLost(object sender, TagEventArgs e)
        //{
        //    RfidItem rfiditem = rfidList.Find(qq => qq.RfidTag == e.Tag);

        //    if (mainWindowEnabled.isAppEnabled != false)
        //    {
        //        string s = string.Format("User logout - {0}.", rfiditem.Name);
        //        mainWindowEnabled.isAppEnabled = false;
        //        List<MotorLogic> MovingMotorsList = Sys.StaticMotorList.FindAll(t => t.Can == true && t.Brake == false);
        //        if (MovingMotorsList.Count > 0)
        //        {
        //            s += " Stopping motors.";

        //            foreach (MotorLogic motor in MovingMotorsList)
        //            {
        //                UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 2);
        //                //UdpTx.UniqueInstance.SendPDOSBC((byte)motor.CanChannel, (byte)motor.SecBrkControlAddress, 0);
        //                UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, 50, 2);
        //                s += string.Format(" Motor={0}, ID={1}, CP={2}, CV={3};", motor.Title, motor.MotorID, motor.CP, motor.CV);
        //            }
        //        }
        //        MainWindow.mainWindowDispacher.Invoke(DispatcherPriority.Normal, (Action)delegate() { Log.Write(s + string.Format(" rfid={0}", rfiditem.RfidTag)); AlertLogic.Add(s); });
        //    }
        //}

        //ADDING INTERFACE KIT
        // 
        void ifKit_Attach(object sender, AttachEventArgs e)
        {
            //tempPhidgetItem = new PhidgetItem();
            //tempPhidgetItem.ifkit = (InterfaceKit)sender;
            InterfaceKit tttt = sender as InterfaceKit;

            if (tttt.SerialNumber == AnalogUpDownLeftRight)
            {
                Ph888_3_Analog_Cursors = tttt;
                Ph888_3_Analog_Cursors.outputs[AIO.Default.SelectGroup_888] = true;
            }

            InterfaceKitList.Add(tttt);

            //JOYSTICKS 
            // checking if interface sensor kit analog inputs work (sensors)
            if (tttt.sensors.Count > 0)
                foreach (StJoystick joystick in Sys.joystickList) joystick.SetJoystickDirection();
            else
            { AlertLogic.Add("No joystick analog inputs detected - phidget interface kit error");  }

        }

        void rfid_Attach(object sender, AttachEventArgs e)
        {
            try
            {
                rfid.outputs[0] = false;
                rfid.outputs[1] = false;
                rfid.Antenna = true;
                string s = "";
                foreach (var t in Sys.rfidList) s += string.Format("{0} {1};", t.Name, t.RfidTag);
                Log.Write("PhRfid xml read: " + s, EventLogEntryType.Information);
            }
            catch (Exception exc) { Log.Write(string.Format("rfid_attach error: {0}", exc.Message), EventLogEntryType.Error); }
        }

        #endregion

        void ifKit_SensorChange(object sender, SensorChangeEventArgs e)
        {

            //if (e.Index == 2) Sys.potSvRotInner = (int)(e.Value / 10);
            //if (e.Index == 3) Sys.potSvRotOuter = (int)(e.Value / 10);

            //if (e.Index == 2) MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate() { Sys.vagoniRxTx.SvRotInner = (int)(e.Value / 10); });
            //if (e.Index == 3) MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate() { Sys.vagoniRxTx.SvRotOuter = (int)(e.Value / 10); });
            //if (e.Index == 2) Sys.vagoniRxTx.SvRotInner = (int)(e.Value / 10);
            //if (e.Index == 3) Sys.vagoniRxTx.SvRotOuter = (int)(e.Value / 10);

            //deo za inicijalizaciju 
            // na pocetku broji sve analogne ulaze da ne bi pucao mora da se proveri da li je raw ulaz razlicit od nule -raw nedostupan !!!!

            //sensor change
            StJoystick joystick = Sys.joystickList.Find(q => q.JIndex == e.Index);

            if (joystick == null || Sys.StaticGroupList.Count - 1 < e.Index) return;// wth???? 20110220

            // phidget joystick 8/8/8 set y axis
            joystick.JV_Raw = e.Value;

            //  Debug.WriteLine(string.Format("raw,norm={0},{1}", e.Value, "bla")); // StJoystick.JoyNormalized(e.Value)


            //phJoystickItem = JoystickPhidget.phJoysticks.Find(qq => qq.JIndex == e.Index);
            //Console.WriteLine(string.Format("{0} joyval {1}  mvpos={2}/{4} mvneg={3}/{5}", DateTime.Now.ToLongTimeString(), e.Value, e.Value * PhdJoystickItem.k + PhdJoystickItem.n1, e.Value * PhdJoystickItem.k + PhdJoystickItem.n2, PhdJoystickItem.JoyNormalized(e.Value), PhdJoystickItem.JoyNormalized(e.Value)));
            //if (phJoystickItem == null) return;
            //GroupLogic group = Sys.StaticGroupList[joystick.Group];
            //if (group == null) return;//skinut uslov da se joy obradjuje samo kad je pritisnuto dugme
            //motorsList = group.MotorsInGroup;
            //if (motorsList.Count == 0) return;
            //if (!Sys.StaticGroupList[phJoystickItem.Group].Trip || !Sys.StaticGroupList[phJoystickItem.Group].Local || Sys.StaticGroupList[phJoystickItem.Group].Can)
            // todo18062010 trip checked by group logic?????? - pa ako ovo ne uradis joystick ce pokusati da pokrene grupu

            //if (mainWindowEnabled.isAppEnabled == false)
            //{
            //    joystick.JV = 0;
            //    joystick.Direction = CanSett.CanCommandStop;
            //    foreach (var moto in Sys.StaticMotorList) moto.JV = 0;
            //    return;
            //}

            ////oreginal
            //if (e.Value < StJoystick.JMeasuredMid - StJoystick.JDeadZone)
            //{
            //    joystick.MV = joystick.JoyNormalized(e.Value);
            //    joystick.Direction = joystick.NegDirection;

            //}
            //else if (e.Value > StJoystick.JMeasuredMid + StJoystick.JDeadZone)
            //{
            //    //joystick.MV = StJoystick.JoyNormalized(e.Value);
            //    joystick.MV = joystick.JoyNormalized(e.Value);
            //    joystick.Direction = joystick.PosDirection;
            //
            //else
            //{
            //    joystick.MV = 0;
            //    joystick.Direction = CanSett.CanCommandStop;
            //}


            //foreach (var moto in motorsList)
            //{
            //    //double vmax = group.IsSync ? motorsList.Min(sdf => sdf.MaxV) : moto.MaxV;
            //    //joystick.JoyNormalized3(e.Value, moto, vmax, joystick);
            //    joystick.JoyNormalized3(e.Value, moto, moto.MaxV, joystick);
            //}

            //Debug.WriteLine(string.Format("raw={0}, joy.mv={1}, direction={2}, lo/hi={3}/{4}", e.Value, joystick.MV, joystick.Direction, StJoystick.JMeasuredMid - StJoystick.JDeadZone, StJoystick.JMeasuredMid + StJoystick.JDeadZone));

        }

        //object objekt;
        //INPUT
        void ifKit_InputChange(object sender, InputChangeEventArgs e)
        {
            if (mainWindowEnabled.isAppEnabled == false) return;

            tempIfKit = sender as InterfaceKit;
            //Debug.WriteLine(DateTime.Now.ToLongTimeString() + " " + tempIfKit.SerialNumber.ToString() + " index=" + e.Index + " value=" + e.Value);
            //motorsList = null;
            //if (mGUI != null)
            //{
            //    motorsList = mGUI._motor.toList();
            //    SPtb = mGUI.SP;
            //    SVtb = mGUI.SV;
            //    MVtb = mGUI.MV;
            //    objekt = mGUI._motor;
            //}
            //if (gGUI != null)
            //{
            //    motorsList = gGUI._group.MotorsInGroup;
            //    SPtb = gGUI.SP;
            //    SVtb = gGUI.SV;
            //    MVtb = gGUI.MV;
            //    objekt = gGUI._group;
            //}
            //if (cmGUI != null)
            //{
            //    motorsList = cmGUI.motor.toList();
            //    SPtb = cmGUI.SP;
            //    SVtb = cmGUI.SV;
            //    objekt = cmGUI.motor;
            //}

            // find joystick
            StJoystick joystick = Sys.joystickList.Find(q => q.JIndex == e.Index);
            if (joystick == null || Sys.StaticGroupList.Count - 1 < e.Index) return;// wth???? 20110220
            // phidget joystick 8/8/8 check button
            joystick.Butt1Pressed = e.Value;

            


            ////deo za joystick
            // if (tempIfKit.SerialNumber == AnalogUpDownLeftRight && Sys.StaticGroupList.Count >= 0) //Joystick          
            //     phANALOG(e.Index, e.Value);

            //if (tempIfKit.SerialNumber == PhidgetCue && (CueLogic.UniqueInstance.TempCueItems.Count > 0
            //    || e.Index == 2 || e.Index == 5 || e.Index == 6 || e.Index == 7 || e.Index == 10 || e.Index == 11 || e.Index == 12 || e.Index == 13 || e.Index == 14 || e.Index == 15) && e.Value) //15 je Prebaci na motor
            //    phCUE(e.Index);


            /* //////////////////////////////2013 remove todo
            if (tempIfKit.SerialNumber == PhidgetCue && e.Value) //15 je Prebaci na motor
                phCUE(e.Index);

            if ((motorsList != null && (mGUI != null || gGUI != null || cmGUI != null)) || (e.Index == 10 || e.Index == 11 || e.Index == 12 || e.Index == 13 || e.Index == 14 || e.Index == 15) && e.Value)
            {
                if (tempIfKit.SerialNumber == PhidgetMotorAndPreviews)
                    MotorsAndPreviews(motorsList, e.Index, e.Value, objekt);
            }


            if (tempIfKit.SerialNumber == PhidgetNumbers && e.Value)
                Numbers(e.Index);
            */
        }

        #region ERRORS
        //ERROR
        void ifKit_Error(object sender, ErrorEventArgs e)
        {
            InterfaceKit tIfKit = sender as InterfaceKit;//todo puca ovde cesto!!!!!
            //MainWindow.mainWindowDispacher.Invoke
            //(DispatcherPriority.Normal, (Action)delegate()
            {
                Console.WriteLine(string.Format("PHIDGET ERROR: {0}, {1}, {2} - {3} {4} {5}", tIfKit.ID, tIfKit.Name, DateTime.Now.ToLongTimeString(), e.Description, e.Code, e.exception.Code, e.exception.Data, e.exception.Description, e.exception.Message));
            }

            //);
        }

        //DETACH
        void ifKit_Detach(object sender, DetachEventArgs e)
        {
            InterfaceKit ik = sender as InterfaceKit;
            if (ik == null) return;
            ik.close();
            InterfaceKitList.Remove(ik);

            //tempPhidgetItem = ListofPhidgetItems.Find(qq => qq.ifkit.SerialNumber == ((InterfaceKit)sender).SerialNumber);
            //if (tempPhidgetItem != null)
            //{
            //    tempPhidgetItem.ifkit.close();
            //    ListofPhidgetItems.Remove(tempPhidgetItem);
            //}

        }

        //ERROR
        void rfid_Error(object sender, ErrorEventArgs e)
        {
            throw new NotImplementedException();
        }



        //DETACH
        void rfid_Detach(object sender, DetachEventArgs e)
        {
            //MessageBox.Show(e.Device.Name);
        }
        #endregion



        #region UniqueInstance
        class PhidgetsInterfacesCreator
        {
            static PhidgetsInterfacesCreator() { }
            internal static readonly PhidgetsInterfaces uniqueInstance = new PhidgetsInterfaces();
        }
        public static PhidgetsInterfaces UniqueInstance
        {
            get { return PhidgetsInterfacesCreator.uniqueInstance; }
            // get { return null; }//2017 terazije nema phidg
        }

        public void initPhidget() { }


        #endregion
        #region INotifyPropertyChanged Members //todo 20130110 da li je ovo potrebno
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(parameter));
        }
        #endregion


    }





}









﻿using StClient.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using SharpDX.DirectInput;
using System.Windows.Threading;
using System.Diagnostics;
using LogAlertHB;

namespace StClient
{
    public class StJoystick //: IMv
    {
        int _group = 0, _jindex = 0, _deadZone = 0, posdirection, negdirection,
            _direction = Properties.CanSettings.Default.CanCommandStop;
        bool _isInverted = false;
        double _jv, _jvPrevious;
        Guid _jguid;

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        [XmlAttribute("Group")]
        public int Group { get { return _group; } set { _group = value; } }

        [XmlAttribute("JGUID")]
        public Guid JGUID { get { return _jguid; } set { _jguid = value; } }
        [XmlAttribute("JIndex")]

        // JIndex is used in phidget 8/8/8 , it is port number for wires 
        public int JIndex { get { return _jindex; } set { _jindex = value; } }

        //[XmlAttribute("DeadZone")]
        //public int DeadZone { get { return _deadZone; } set { _deadZone = value; } }

        [XmlAttribute("IsInverted")]
        public bool IsInverted { get { return _isInverted; } set { _isInverted = value; } }

        [XmlIgnore]
        public bool Butt1Pressed { get; set; }

        [XmlIgnore]
        public Joystick _joystickDx { get; private set; }
        [XmlIgnore]
        public GroupLogic _groupLogic { get; set; }

        [XmlIgnore]
        public int PosDirection { get { return posdirection; } set { posdirection = value; } }
        [XmlIgnore]
        public int NegDirection { get { return negdirection; } set { negdirection = value; } }

        [XmlIgnore]
        //public static double JMeasuredNeg = 160;//20140522 todo staviti ovo u config JoystickMeasuredNeg
        public static double JMeasuredNeg = Settings.Default.JoystickMeasuredNeg;//20140522 todo staviti ovo u config JoystickMeasuredNeg
        [XmlIgnore]
        //public static double JMeasuredPos = 840;
        public static double JMeasuredPos = Settings.Default.JoystickMeasuredPos;
        [XmlIgnore]
        //public static double JMeasuredMid = 500;
        public static double JMeasuredMid = Settings.Default.JoystickMeasuredMid;
        [XmlIgnore]
        public static double JDeadZone = Settings.Default.JoystickDeadZone;
        //[XmlIgnore]
        //public static int n1 = -100;
        //[XmlIgnore]
        //public static double n2 = -100F * (500 + Settings.Default.JoystickDeadZone) / (500 - Settings.Default.JoystickDeadZone);
        //[XmlIgnore]
        ////public static int low = 500 - Settings.Default.JoystickDeadZone;
        //public static int low = 500 - Convert.ToInt32(JDeadZone);//todo 20131006 jdeadzone=0 ako se inicijalizuje posle low, tj zavisi od redosleda operacija. PROMENI TODO
        //[XmlIgnore]
        ////public static int hi = 500 + Settings.Default.JoystickDeadZone;
        //public static int hi = 500 + Convert.ToInt32(JDeadZone);
        //[XmlIgnore]
        //public static double k = (100F / (500 - Settings.Default.JoystickDeadZone));



        //[XmlIgnore]
        //public static double x1Neg = JMeasuredNeg;
        //[XmlIgnore]
        //public static double x2Neg = JMeasuredMid - JDeadZone;
        //[XmlIgnore]
        //public double y1Neg = 100;
        //[XmlIgnore]
        //public static double y2Neg = 0;
        //[XmlIgnore]
        //public static double x1Pos = JMeasuredMid + JDeadZone;
        //[XmlIgnore]
        //public static double x2Pos = JMeasuredPos;
        //[XmlIgnore]
        //public static double y1Pos = 0;
        //[XmlIgnore]
        //public double y2Pos = 100;
        //[XmlIgnore]
        //public double kNeg { get { return (y2Neg - y1Neg) / (x2Neg - x1Neg); } }
        //[XmlIgnore]
        //public double nNeg { get { return y2Neg - (kNeg * x2Neg); } }
        //[XmlIgnore]
        //public double kPos { get { return -kNeg; } }
        //[XmlIgnore]
        //public double nPos { get { return y2Pos - (kPos * x2Pos); } }

        //public void SetJoystickMaxV(double _maxV) { y1Neg = y2Pos = _maxV; }

        //public double JoyNormalized3(double Win, MotorLogic moto, double _vmax, StJoystick joystick)
        //public double JoyNormalized4(GroupLogic grp)
        //{
        //    //double Win2 = Math.Abs(Win - JMeasuredMid);
        //    double Vmax = _vmax;
        //    double k = (Vmax - 0) / (JMeasuredPos - (JMeasuredMid + JDeadZone));
        //    double nPos = -k * (JMeasuredMid + JDeadZone);
        //    double nNeg = k * (JMeasuredMid - JDeadZone);
        //    double WinA = JMeasuredNeg;//65536
        //    double WinB = JMeasuredMid - JDeadZone;//63000
        //    double WinC = JMeasuredMid;//32768
        //    double WinD = JMeasuredMid + JDeadZone;
        //    double WinE = JMeasuredPos;//0

        //    double Wout;
        //    double Win = JV_Raw;

        //    if (JMeasuredNeg > JMeasuredPos)//todo clean this (0..65536 and 65536..0)
        //    {
        //        Win = JMeasuredNeg - JV_Raw;
        //        k = (Vmax - 0) / (JMeasuredNeg - (JMeasuredMid + JDeadZone));
        //        nPos = -k * (JMeasuredMid + JDeadZone);
        //        nNeg = k * (JMeasuredMid - JDeadZone);
        //        WinA = JMeasuredPos;//65536
        //        WinB = JMeasuredMid - JDeadZone;//63000
        //        WinC = JMeasuredMid;//32768
        //        WinD = JMeasuredMid + JDeadZone;
        //        WinE = JMeasuredNeg;//0

        //    }
        //    if ((Win > WinB) && (Win < WinD))
        //    { Wout = 0; moto.Direction = CanSett.CanCommandStop; }
        //    else if (Win <= WinA)
        //    { Wout = Vmax; moto.Direction = moto.NegDirection; }
        //    else if (Win >= WinE)
        //    { Wout = Vmax; moto.Direction = moto.PosDirection; }
        //    else if (Win > WinA && Win <= WinB)
        //    { Wout = -k * Win + nNeg; moto.Direction = moto.NegDirection; }
        //    else if (Win < WinE && Win >= WinD)
        //    { Wout = k * Win + nPos; moto.Direction = moto.PosDirection; }
        //    else
        //    { Wout = double.NaN; }
        //    Debug.WriteLine(string.Format("joy id={0,2} raw={1,6} in={2,6} out={3,6:0.00} dir={4,16}",
        //        JIndex, JV_Raw, Win, Wout, moto.Direction), EventLogEntryType.Information);

        //    return moto.TV = Wout;
        //}
        public double JoyNormalized3(MotorLogic moto, double _vmax)
        {
            //double Win2 = Math.Abs(Win - JMeasuredMid);
            double Vmax = _vmax;
            double k = (Vmax - 0) / (JMeasuredPos - (JMeasuredMid + JDeadZone));
            double nPos = -k * (JMeasuredMid + JDeadZone);
            double nNeg = k * (JMeasuredMid - JDeadZone);
            double WinA = JMeasuredNeg;//65536
            double WinB = JMeasuredMid - JDeadZone;//63000
            double WinC = JMeasuredMid;//32768
            double WinD = JMeasuredMid + JDeadZone;
            double WinE = JMeasuredPos;//0

            double Wout;
            double Win = JV_Raw;

            if (JMeasuredNeg > JMeasuredPos)//todo clean this (0..65536 and 65536..0)
            {
                Win = JMeasuredNeg - JV_Raw;
                k = (Vmax - 0) / (JMeasuredNeg - (JMeasuredMid + JDeadZone));
                nPos = -k * (JMeasuredMid + JDeadZone);
                nNeg = k * (JMeasuredMid - JDeadZone);
                WinA = JMeasuredPos;//65536
                WinB = JMeasuredMid - JDeadZone;//63000
                WinC = JMeasuredMid;//32768
                WinD = JMeasuredMid + JDeadZone;
                WinE = JMeasuredNeg;//0

            }
            if ((Win > WinB) && (Win < WinD))
            { Wout = 0; moto.Direction = CanSett.CanCommandStop; }
            else if (Win <= WinA)
            { Wout = Vmax; moto.Direction = moto.NegDirection; }
            else if (Win >= WinE)
            { Wout = Vmax; moto.Direction = moto.PosDirection; }
            else if (Win > WinA && Win <= WinB)
            { Wout = -k * Win + nNeg; moto.Direction = moto.NegDirection; }
            else if (Win < WinE && Win >= WinD)
            { Wout = k * Win + nPos; moto.Direction = moto.PosDirection; }
            else
            { Wout = double.NaN; }
            //Debug.WriteLine(string.Format("joy id={0,2} raw={1,6} in={2,6} out={3,6:0.00} dir={4,16}", JIndex, JV_Raw, Win, Wout, moto.Direction), EventLogEntryType.Information);

            return moto.TV = Wout;
        }

        [XmlIgnore]
        public int Direction { get { return _direction; } set { _direction = value; } }
        [XmlIgnore]
        public double JV { get { return _jv; } set { _jv = value; } }
        [XmlIgnore]
        public double JV_Raw { get; set; }
        [XmlIgnore]
        public double JV_Previous { get; set; }


        public void SetJoystickDirection()
        {
            PosDirection = (IsInverted ? CanSett.CanCommandManNegative : CanSett.CanCommandManPositive);
            NegDirection = (IsInverted ? CanSett.CanCommandManPositive : CanSett.CanCommandManNegative);
        }

        public void SetDxJoystick(Joystick _tempJoy) { _joystickDx = _tempJoy; }
    }

}

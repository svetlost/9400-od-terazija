﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Windows.Threading;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Windows;
using LogAlertHB;

namespace StClient
{
    [Serializable]
    struct CanOverUdp
    {
        public DateTime timestamp;
        public byte[] Data;
    }

    public class PrioritySdoMsg
    {
        public int MotorID;
        public DateTime TimeOutStamp;
        public int Index;
        public byte SubIndex;
        public SdoPriority Priority;
        public byte[] Data;
        public SdoType type;
        public bool SdoSent = false;
        public bool MarkedForDeletion = false;
    }



    class UdpTx : IInitResult
    {
        List<CanOverUdp> delaySendList = new List<CanOverUdp>();
        static IPEndPoint ipepServer = new IPEndPoint(IPAddress.Parse(Properties.Settings.Default.ServerMainIP), Properties.Settings.Default.ServerPort);
        static Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        CanOverUdp tempMsg = new CanOverUdp();
        DispatcherTimer UdpDelayedSendTimer = new DispatcherTimer();
        //DispatcherTimer SendSDO = new DispatcherTimer();
        //DispatcherTimer SendSDOStrujaTimer = new DispatcherTimer();

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        //SUMS Panels
        CounterSumsLogic TXtotal = new CounterSumsLogic(), UdpTxError = new CounterSumsLogic();
        //CounterSumsLogic TXtotal = new CounterSumsLogic(), HiSdoTimeOut = new CounterSumsLogic(), MedSdoTimeOut = new CounterSumsLogic(), LowSdoTimeOut = new CounterSumsLogic(), UdpTxError = new CounterSumsLogic();

        //static int testTimeout_ms = 1000;
        //List<PrioritySdoMsg> PrioritySdoList = new List<PrioritySdoMsg>();


        UdpTx()
        {
            UdpDelayedSendTimer.Interval = TimeSpan.FromMilliseconds(Properties.Settings.Default.UdpSendInterval);
            UdpDelayedSendTimer.Tick += new EventHandler(UdpDelayedSend);
            UdpDelayedSendTimer.Start();

            //SendSDO.Interval = TimeSpan.FromMilliseconds(20);//20110221 bilo=20. todo stavii u config
            //SendSDO.Tick += new EventHandler(SendSDO_Tick);
            //SendSDO.Start();

            //SendSDOStrujaTimer.Interval = TimeSpan.FromSeconds(1);
            //SendSDOStrujaTimer.Tick += new EventHandler(SendSDOStrujaTimer_Tick);
            //SendSDOStrujaTimer.Start(); //XXX

            MainWindow.mainWindowDispacher.BeginInvoke(DispatcherPriority.Normal, (Action)delegate ()
                          {
                              SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(TXtotal), 4);
                              SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(UdpTxError), 3);
                              //SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(HiSdoTimeOut), 5);
                              //SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(MedSdoTimeOut), 6);
                              //SumsPPsPanel.Instance.AddPanel(new CountersSumsPPS(LowSdoTimeOut), 7);
                              UdpTxError.Names = "Udp Tx Error";
                              TXtotal.Names = "TX Total";
                              //HiSdoTimeOut.Names = "Sdo Timeout Hi Priority:";
                              //MedSdoTimeOut.Names = "Sdo Timeout Med Priority:";
                              //LowSdoTimeOut.Names = "Sdo Timeout Low Priority:";
                          });
            initResult += string.Format("TxSck set to {0}:{1}", ipepServer.Address, ipepServer.Port);

        }

        class UdpTxCreator
        {
            static UdpTxCreator() { }
            internal static readonly UdpTx uniqueInstance = new UdpTx();
        }
        public static UdpTx UniqueInstance
        {
            get { return UdpTxCreator.uniqueInstance; }
        }

        //void SendSDOStrujaTimer_Tick(object sender, EventArgs e)
        //{
        //    foreach (MotorLogic m in Sys.StaticMotorList.FindAll(q => q.Can == true))
        //        SendSDOReadRQ(m.MotorID, CanSett.ImotIndex, CanSett.ImotSubIndex, SdoPriority.med);
        //}

        //void SendSDO_Tick(object sender, EventArgs e)
        //{
        //    //remove timedout
        //    string messages = "";

        //    if (PrioritySdoList.Count == 0) return;

        //    DateTime dt_now = DateTime.Now;
        //    //lock (PrioritySdoList)
        //    //foreach (PrioritySdoMsg psdo in PrioritySdoList)
        //    for (int i = 0; i < PrioritySdoList.Count; i++)
        //    {
        //        if (PrioritySdoList[i].SdoSent == true && PrioritySdoList[i].TimeOutStamp < dt_now)
        //        {
        //            switch (PrioritySdoList[i].Priority)
        //            {
        //                case SdoPriority.hi: HiSdoTimeOut.Count++; break;
        //                case SdoPriority.med: MedSdoTimeOut.Count++; break;
        //                case SdoPriority.lo: LowSdoTimeOut.Count++; break;
        //            }
        //            //PrioritySdoList.Remove(psdo);
        //            PrioritySdoList[i].MarkedForDeletion = true;
        //            messages += " MotorID: " + PrioritySdoList[i].MotorID.ToString() + " Index:" + PrioritySdoList[i].Index.ToString() + " SubIndex:" + PrioritySdoList[i].SubIndex.ToString();
        //        }
        //    }

        //    //PrioritySdoList.RemoveAll(t => t.MarkedForDeletion);
        //    for (int i = 0; i < PrioritySdoList.Count; i++) if (PrioritySdoList[i].MarkedForDeletion) PrioritySdoList.RemoveAt(i);

        //    if (messages != "")
        //    {
        //        //Log.Write("UdpTx - Messages ack not recived" + messages);
        //        Log.Write("UdpTx - Msg ack not recived" + messages, EventLogEntryType.Warning);
        //        AlertLogic.Add("UdpTx - Msg ack not recived" + messages);
        //    }

        //    foreach (MotorLogic m in Sys.StaticMotorList)
        //    {
        //        int a1 = 0, a2 = 0;
        //        PrioritySdoMsg tempMsg = null;
        //        if (m.Can == true)
        //        {
        //            //a1 = PrioritySdoList.Count(c => c.MotorID == m.MotorID);
        //            //a2 = PrioritySdoList.Count(c => c.MotorID == m.MotorID && c.SdoSent == true);
        //            for (int i = 0; i < PrioritySdoList.Count; i++)
        //            {
        //                if (PrioritySdoList[i].MotorID == m.MotorID) a1++;
        //                if (PrioritySdoList[i].MotorID == m.MotorID && PrioritySdoList[i].SdoSent == true) a2++;
        //            }
        //            if (a1 == 0) continue; //any msgs waiting?
        //            if (a2 > 0) continue; //already waiting for reply - no sending

        //            DateTime lkj = new DateTime(2008, 3, 9, 16, 5, 7, 123);

        //            //DateTime temptime = DateTime.Now;
        //            //no msgs waiting for ack/rsp, send hi priority
        //            //foreach (PrioritySdoMsg psdo in PrioritySdoList)
        //            for (int i = 0; i < PrioritySdoList.Count; i++)
        //            {
        //                if (PrioritySdoList[i].MotorID == m.MotorID && PrioritySdoList[i].Priority == SdoPriority.hi)
        //                {
        //                    if (PrioritySdoList[i].TimeOutStamp >= dt_now)
        //                    {
        //                        lkj = PrioritySdoList[i].TimeOutStamp;
        //                        dt_now = PrioritySdoList[i].TimeOutStamp;
        //                    }
        //                }
        //            }
        //            if (lkj == new DateTime(2008, 3, 9, 16, 5, 7, 123))
        //            {
        //                //foreach (PrioritySdoMsg psdo in PrioritySdoList)
        //                for (int i = 0; i < PrioritySdoList.Count; i++)
        //                {
        //                    if (PrioritySdoList[i].MotorID == m.MotorID && PrioritySdoList[i].Priority == SdoPriority.med)
        //                    {
        //                        if (PrioritySdoList[i].TimeOutStamp >= dt_now)
        //                        {
        //                            lkj = PrioritySdoList[i].TimeOutStamp;
        //                            dt_now = PrioritySdoList[i].TimeOutStamp;
        //                        }
        //                    }
        //                }
        //            }
        //            if (lkj == new DateTime(2008, 3, 9, 16, 5, 7, 123))
        //            {
        //                //foreach (PrioritySdoMsg psdo in PrioritySdoList)
        //                for (int i = 0; i < PrioritySdoList.Count; i++)
        //                {
        //                    if (PrioritySdoList[i].MotorID == m.MotorID && PrioritySdoList[i].Priority == SdoPriority.lo)
        //                    {
        //                        if (PrioritySdoList[i].TimeOutStamp >= dt_now)
        //                        {
        //                            lkj = PrioritySdoList[i].TimeOutStamp;
        //                            dt_now = PrioritySdoList[i].TimeOutStamp;
        //                        }
        //                    }
        //                }
        //            }
        //            if (lkj == new DateTime(2008, 3, 9, 16, 5, 7, 123)) continue;

        //            //foreach (PrioritySdoMsg psdo in PrioritySdoList)
        //            for (int i = 0; i < PrioritySdoList.Count; i++)
        //            {
        //                if (PrioritySdoList[i].TimeOutStamp == lkj && PrioritySdoList[i].MotorID == m.MotorID)
        //                {
        //                    tempMsg = PrioritySdoList[i];
        //                }
        //            }

        //            //if (tempMsg == null)
        //            //{
        //            //    Debug.WriteLine("nesto ozbiljno nevalja");
        //            //}

        //            byte[] t0 = new byte[CanSett.CanOverUdpMessageLength];
        //            byte[] t1 = BitConverter.GetBytes(CanSett.LenzeIndexConversionConstant - tempMsg.Index);////////////TODO CHECKTHISOUT

        //            t0[1] = t1[0];
        //            t0[2] = t1[1];
        //            t0[3] = tempMsg.SubIndex;
        //            t0[8] = m.CanChannel;
        //            t0[9] = 8; //dlc
        //            t0[10] = m.SDO_CanMsgID[0];
        //            t0[11] = m.SDO_CanMsgID[1];

        //            if (tempMsg.type == SdoType.write)
        //            {
        //                t0[0] = 35;//sdo command write
        //                t0[4] = tempMsg.Data[0];
        //                t0[5] = tempMsg.Data[1];
        //                t0[6] = tempMsg.Data[2];
        //                t0[7] = tempMsg.Data[3];
        //            }
        //            else if (tempMsg.type == SdoType.read)
        //                t0[0] = 64;//sdo command read

        //            SendUDP(t0);

        //            //Debug.WriteLine(string.Format("{0} {1} {2}", DateTime.Now.ToLongTimeString(), t0[10], t0[11]));

        //            tempMsg.SdoSent = true;
        //        }
        //    }

        //}

        //TODO prekontrolisati SdoResponseReceived()???? darko 08082010
        //public void SdoResponseReceived(int motorID, int index, byte subindex, byte[] data)
        //{
        //    foreach (PrioritySdoMsg psdo in PrioritySdoList)
        //    {
        //        if (psdo.MotorID == motorID && psdo.Index == index && psdo.SubIndex == subindex && psdo.SdoSent == true)
        //            psdo.MarkedForDeletion = true;
        //    }
        //}

        public void SendPDO(int motorID, int data, double sendDelayInterval, byte dlc)
        {
            MotorLogic tempMotor = Sys.StaticMotorList.Find(m => m.MotorID == motorID);
            //if (tempMotor.Can != true) return;

            byte[] temp = new byte[CanSett.CanOverUdpMessageLength];

            temp[0] = BitConverter.GetBytes(data)[0];
            temp[1] = BitConverter.GetBytes(data)[1];
            temp[2] = BitConverter.GetBytes(data)[2];
            temp[3] = BitConverter.GetBytes(data)[3];
            temp[4] = 0;
            temp[5] = 0;
            temp[6] = 0;
            temp[7] = 0;
            temp[8] = tempMotor.CanChannel;
            temp[9] = dlc;
            temp[10] = tempMotor.PDO_CanMsgID[0];
            temp[11] = tempMotor.PDO_CanMsgID[1];

            if (sendDelayInterval == 0)
            {
                SendUDP(temp);
                //if (tempMotor.MotorID == 1) Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} pdo sent data:{1}", DateTime.Now, data));
            }
            else
            {
                tempMsg.Data = temp;
                tempMsg.timestamp = DateTime.Now + TimeSpan.FromMilliseconds(sendDelayInterval);
                delaySendList.Add(tempMsg);
                //if (tempMotor.MotorID == 1) Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} delay pdo sent data:{1} delay:{2:hh:mm:ss.fff}", DateTime.Now, data, tempMsg.timestamp));
            }
            //if (tempMotor.MotorID == 8)
            //{
            //    Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} pdo sent motorID:{1,6} delay:{2,6} data:{3,16}",
            //        DateTime.Now, motorID, sendDelayInterval, data));
            //    //Utl.printStack("motor id008");
            //}
        }
        public void SendPDO_ResRef(int motorID, int data, double sendDelayInterval, byte dlc)//20170903 mora da ide na pdo2!!!
        {
            MotorLogic tempMotor = Sys.StaticMotorList.Find(m => m.MotorID == motorID);
            //if (tempMotor.Can != true) return;

            byte[] pdo2 = BitConverter.GetBytes(CanSett.PDO2_TX_BaseID + tempMotor.CanAddress);
            byte[] temp = new byte[CanSett.CanOverUdpMessageLength];

            temp[0] = BitConverter.GetBytes(data)[0];
            temp[1] = BitConverter.GetBytes(data)[1];
            temp[2] = BitConverter.GetBytes(data)[2];
            temp[3] = BitConverter.GetBytes(data)[3];
            temp[4] = 0;
            temp[5] = 0;
            temp[6] = 0;
            temp[7] = 0;
            temp[8] = tempMotor.CanChannel;
            temp[9] = dlc;
            temp[10] = pdo2[0];
            temp[11] = pdo2[1];

            if (sendDelayInterval == 0)
            {
                SendUDP(temp);
                //if (tempMotor.MotorID == 1) Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} pdo sent data:{1}", DateTime.Now, data));
            }
            else
            {
                tempMsg.Data = temp;
                tempMsg.timestamp = DateTime.Now + TimeSpan.FromMilliseconds(sendDelayInterval);
                delaySendList.Add(tempMsg);
                //if (tempMotor.MotorID == 1) Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} delay pdo sent data:{1} delay:{2:hh:mm:ss.fff}", DateTime.Now, data, tempMsg.timestamp));
            }
            //if (tempMotor.MotorID == 8)
            //{
            //    Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} pdo sent motorID:{1,6} delay:{2,6} data:{3,16}",
            //        DateTime.Now, motorID, sendDelayInterval, data));
            //    //Utl.printStack("motor id008");
            //}
        }
        public void SendPDORaw(int CanMsgCobID, byte CanNetworkID, byte[] data, double sendDelayInterval, byte dlc)
        {
            byte[] temp = new byte[CanSett.CanOverUdpMessageLength];
            byte[] msgId = BitConverter.GetBytes(CanMsgCobID);

            temp[0] = data[0];
            temp[1] = data[1];
            temp[2] = data[2];
            temp[3] = data[3];
            temp[4] = data[4];
            temp[5] = data[5];
            temp[6] = data[6];
            temp[7] = data[7];
            temp[8] = CanNetworkID;
            temp[9] = dlc;
            temp[10] = msgId[0];
            temp[11] = msgId[1];

            if (sendDelayInterval == 0) SendUDP(temp);
            else
            {
                tempMsg.Data = temp;
                tempMsg.timestamp = DateTime.Now + TimeSpan.FromMilliseconds(sendDelayInterval);
                delaySendList.Add(tempMsg);
            }
        }

        public void SendSDOReadRQ(MotorLogic m, int index, byte subindex, TimeSpan sendDelayInterval)
        {
            byte[] t0 = new byte[CanSett.CanOverUdpMessageLength];
            byte[] t1 = BitConverter.GetBytes(CanSett.LenzeIndexConversionConstant - index);////////////TODO CHECKTHISOUT

            t0[1] = t1[0];
            t0[2] = t1[1];
            t0[3] = subindex;
            t0[8] = m.CanChannel;
            t0[9] = 8; //dlc
            t0[10] = m.SDO_CanMsgID[0];
            t0[11] = m.SDO_CanMsgID[1];

            t0[0] = 64;//sdo command read

            if (sendDelayInterval == TimeSpan.FromMilliseconds(0)) SendUDP(t0);
            else
            {
                tempMsg.Data = t0;
                tempMsg.timestamp = DateTime.Now + sendDelayInterval;
                delaySendList.Add(tempMsg);
            }
        }
        //OREGINAL 20131012
        //public void SendSDOReadRQ(int motorID, int index, byte subindex, SdoPriority priority)
        //{
        //    SdoTimeOut temp;
        //    switch (priority)
        //    {
        //        case SdoPriority.hi: temp = SdoTimeOut.hi; break;
        //        case SdoPriority.med: temp = SdoTimeOut.med; break;
        //        default: temp = SdoTimeOut.lo; break;
        //    }
        //    //lock (PrioritySdoList)
        //    {
        //        if (PrioritySdoList.Count(ttt => ttt.Index == index && ttt.MotorID == motorID && ttt.SubIndex == subindex) == 0)
        //        {

        //            PrioritySdoList.Add(new PrioritySdoMsg()
        //            {
        //                MotorID = motorID,
        //                Data = new byte[] { 0, 0, 0, 0 },
        //                Index = index,
        //                SubIndex = subindex,
        //                TimeOutStamp = DateTime.Now + TimeSpan.FromMilliseconds((int)temp),
        //                Priority = priority,
        //                type = SdoType.read
        //            });
        //        }
        //    }
        //}

        //izbacen priority 20131012
        //public void SendSDOReadRQ(int motorID, int index, byte subindex, SdoPriority priority)
        //{
        //    MotorLogic ml = Sys.StaticMotorList.Find(q => q.MotorID == motorID);
        //    if (ml == null) return;//todo handle this 20131012

        //    byte[] t0 = new byte[CanStructure.CanOverUdpMessageLength];
        //    byte[] t1 = BitConverter.GetBytes(CanStructure.LenzeIndexConversionConstant - index);////////////TODO CHECKTHISOUT

        //    t0[1] = t1[0];
        //    t0[2] = t1[1];
        //    t0[3] = subindex;
        //    t0[8] = ml.CanChannel;
        //    t0[9] = 8; //dlc
        //    t0[10] = ml.SDO_CanMsgID[0];
        //    t0[11] = ml.SDO_CanMsgID[1];

        //    t0[0] = 64;//sdo command read
        //    t0[4] = t0[5] = t0[6] = t0[7] = 0;

        //    SendUDP(t0);
        //}


        public void SendSDOWriteRQ(MotorLogic m, int index, byte subindex, double value, TimeSpan delay)
        {
            byte[] t0 = new byte[CanSett.CanOverUdpMessageLength];
            byte[] t1 = BitConverter.GetBytes(CanSett.LenzeIndexConversionConstant - index);////////////TODO CHECKTHISOUT
            byte[] Data = BitConverter.GetBytes(Convert.ToInt32(value * 10000));

            t0[0] = 35;//sdo command write
            t0[1] = t1[0];
            t0[2] = t1[1];
            t0[3] = subindex;
            t0[4] = Data[0];
            t0[5] = Data[1];
            t0[6] = Data[2];
            t0[7] = Data[3];
            t0[8] = m.CanChannel;
            t0[9] = 8; //dlc
            t0[10] = m.SDO_CanMsgID[0];
            t0[11] = m.SDO_CanMsgID[1];

            if (delay.Ticks == 0) SendUDP(t0);
            else
            {
                tempMsg.Data = t0;
                tempMsg.timestamp = DateTime.Now + delay;
                delaySendList.Add(tempMsg);
            }

        }

        //OREGINAL 20131012
        //public void SendSDOWriteRQ(int motorID, int index, byte subindex, double value, SdoPriority priority)
        //{
        //    SdoTimeOut temp;
        //    switch (priority)
        //    {
        //        case SdoPriority.hi: temp = SdoTimeOut.hi; break;
        //        case SdoPriority.med: temp = SdoTimeOut.med; break;
        //        default: temp = SdoTimeOut.lo; break;
        //    }

        //    //lock (PrioritySdoList)
        //    {
        //        PrioritySdoList.Add(new PrioritySdoMsg()
        //        {
        //            MotorID = motorID,
        //            Data = BitConverter.GetBytes(Convert.ToInt32(value * 10000)),
        //            Index = index,
        //            SubIndex = subindex,
        //            TimeOutStamp = DateTime.Now + TimeSpan.FromMilliseconds((int)temp),
        //            Priority = priority,
        //            type = SdoType.write
        //        });
        //    }
        //}
        //public void SendSDOWriteRQ(int motorID, int index, byte subindex, double value, SdoPriority priority)
        //{
        //    MotorLogic ml = Sys.StaticMotorList.Find(q => q.MotorID == motorID);
        //    if (ml == null) return;//todo handle this 20131012

        //    byte[] t0 = new byte[CanStructure.CanOverUdpMessageLength];
        //    byte[] t1 = BitConverter.GetBytes(CanStructure.LenzeIndexConversionConstant - index);////////////TODO CHECKTHISOUT
        //    byte[] _tempMsg = BitConverter.GetBytes(Convert.ToInt32(value * 10000));

        //    t0[1] = t1[0];
        //    t0[2] = t1[1];
        //    t0[3] = subindex;
        //    t0[8] = ml.CanChannel;
        //    t0[9] = 8; //dlc
        //    t0[10] = ml.SDO_CanMsgID[0];
        //    t0[11] = ml.SDO_CanMsgID[1];

        //    t0[0] = 35;//sdo command write
        //    t0[4] = _tempMsg[0];
        //    t0[5] = _tempMsg[1];
        //    t0[6] = _tempMsg[2];
        //    t0[7] = _tempMsg[3];

        //    SendUDP(t0);
        //}



        void UdpDelayedSend(object sender, EventArgs e)
        {
            //todo022011: ovde staviti lock i ne kopirati deo liste nego raditi nad oreginalom???
            DateTime now = DateTime.Now;
            List<CanOverUdp> temp = delaySendList.FindAll(message => (message.timestamp <= now));
            if (temp != null)
            {
                foreach (CanOverUdp msg in temp)
                    SendUDP(msg.Data);

                delaySendList.RemoveAll(message => (message.timestamp <= now));
            }

        }

        void SendUDP(byte[] msg)
        {
            try
            {
                server.SendTo(msg, msg.Length, SocketFlags.None, ipepServer);

                TXtotal.Count++;

                //if (msg[10] == 51 && msg[11] == 3) Sys.printStack(Sys.StaticMotorList.Find(m => m.MotorID == 40));
                //Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} udp sent {1,3} {2,3} {3,3} {4,3} {5,3} {6,3} {7,3} {8,3} {9,3} {10,3} {11,3} {12,3}",
                //    DateTime.Now, msg[0], msg[1], msg[2], msg[3], msg[4], msg[5], msg[6], msg[7], msg[8], msg[9], msg[10], msg[11]));
                //Utl.printStack("udp sent");
            }
            catch (Exception ex)
            {
                UdpTxError.Count++;
                Log.Write("UDPTX-SendUDP ErrorCount= " + UdpTxError.Count + ", ErrorMessage=" + ex.Message, EventLogEntryType.Error);
            }
        }

        internal void SendPDOSBC(byte CanChannel, byte[] PlcAddress, byte[] Output)
        {
            byte[] temp = new byte[CanSett.CanOverUdpMessageLength];
            temp[0] = Output[0];
            temp[1] = Output[1];
            temp[2] = Output[2];
            temp[3] = Output[3];
            temp[4] = Output[4];
            temp[5] = Output[5];
            temp[6] = Output[6];
            temp[7] = Output[7];
            temp[8] = CanChannel;
            temp[9] = 8;//dlc
            temp[10] = PlcAddress[0];
            temp[11] = PlcAddress[1];

            SendUDP(temp);
        }

        public void Send8200PDO(bool Up, bool Down, int CanAdr)
        {

            byte[] bb = BitConverter.GetBytes(CanAdr + 512);

            byte[] temp = new byte[CanSett.CanOverUdpMessageLength];
            //temp[0] = BitConverter.GetBytes(data)[0];
            if (Up == true) temp[0] = 4;
            else if (Down == true) temp[0] = 2;
            else temp[0] = 0;
            temp[1] = temp[2] = temp[3] = temp[4] = temp[5] = temp[6] = temp[7] = 0;
            temp[8] = 2; //can channel
            temp[9] = 8; //dlc
            temp[10] = bb[0];
            temp[11] = bb[1];


            SendUDP(temp);

        }
        public void SendHoistPDO(long data)
        {

            byte[] bb = BitConverter.GetBytes(57 + 768);

            byte[] temp = new byte[CanSett.CanOverUdpMessageLength];

            temp[0] = BitConverter.GetBytes(data)[0];
            temp[1] = BitConverter.GetBytes(data)[1];
            temp[2] = BitConverter.GetBytes(data)[2];
            temp[3] = BitConverter.GetBytes(data)[3];
            temp[4] = temp[5] = temp[6] = temp[7] = 0;
            temp[8] = 0; //can channel
            temp[9] = 8; //dlc
            temp[10] = bb[0];
            temp[11] = bb[1];


            SendUDP(temp);

        }

        public void SendGoOperational(byte canChannel)
        {
            byte[] temp = new byte[CanSett.CanOverUdpMessageLength];
            temp[0] = 1;
            temp[1] = temp[2] = temp[3] = temp[4] = temp[5] = temp[6] = temp[7] = 0;
            temp[8] = canChannel; //can channel
            temp[9] = 2; //dlc
            temp[10] = 0;
            temp[11] = 0;

            SendUDP(temp);
        }

        internal void SendPDO_InitAll()
        {
            byte[] bb = BitConverter.GetBytes(56 + 768);
            byte[] temp = new byte[CanSett.CanOverUdpMessageLength];
            temp[0] = 2; temp[1] = temp[2] = temp[3] = temp[4] = temp[5] = temp[6] = temp[7] = 0;
            temp[8] = 0;//can channel
            temp[9] = 8;//dlc
            temp[10] = bb[0];
            temp[11] = bb[1];

            SendUDP(temp);

            temp[0] = 0; temp[1] = temp[2] = temp[3] = temp[4] = temp[5] = temp[6] = temp[7] = 0;
            TimedAction.ExecuteWithDelay(new Action(delegate
            {
                SendUDP(temp);
            }), TimeSpan.FromSeconds(1));
        }

        public string initResult { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace StClient
{
    public class MotorsXml : ImotorState, IManualVelocity, IPositionVelocitySet, ISyncIMotorsList
    {
        double _GraphMin, _GraphMax;

        int _motorID = 1;
        [XmlAttribute("MotorID")]
        public int MotorID { get { return _motorID; } set { _motorID = value; } }
        [XmlAttribute("CanAddress")]
        public short CanAddress { get { return _canAddress; } set { _canAddress = value; } }
        [XmlAttribute("Title")]
        public string Title { get { return _title; } set { if (_title != value) { _title = value; OnNotify("Title"); } } }
        [XmlAttribute("MaxV")]
        public double MaxV { get { return _MaxV; } set { if (_MaxV != value) { _MaxV = value; OnNotify("MaxV"); } } }
        [XmlAttribute("DefaultMV")]
        public double DefaultMV { get { return _DefaultMV; } set { if (_DefaultMV != value) { _DefaultMV = value; OnNotify("DefaultMV"); } } }
        [XmlAttribute("RampTime")]
        public int RampTime { get { return _rampTime; } set { _rampTime = value; } }
        [XmlAttribute("LimNeg")]
        public double LimNeg { get { return _limNeg; } set { if (_limNeg != value) { _limNeg = value; OnNotify("LimNeg"); } } }
        [XmlAttribute("LimPos")]
        public double LimPos { get { return _limPos; } set { if (_limPos != value) { _limPos = value; OnNotify("LimPos"); } } }
        [XmlAttribute("SDO_Channel")]
        public int SdoChannel { get { return _sdoChannel; } set { _sdoChannel = value; } }
        [XmlAttribute("Enabled")]
        public bool Enabled { get { return _enabled; } set { _enabled = value; } }
        [XmlAttribute("Inverted")]
        public bool Inverted { get { return _inverted; } set { _inverted = value; } }
        [XmlAttribute("CAN_BUS")]
        public byte CanChannel { get { return _canChannel; } set { _canChannel = value; } }
        [XmlAttribute("Coeficient")]
        public double Coeficient { get { return _coeficient; } set { _coeficient = value; } }
        [XmlAttribute("ClusterLogical")]//logical group of drives such as stage platforms, sidestage hoists ...
        public byte ClusterLogical { get { return _ClusterLogical; } set { _ClusterLogical = value; } }
        [XmlAttribute("ClusterElectric")]//electrical circuit that supplies the drive. used for power consumption calc
        public byte ClusterElectric { get { return _ClusterElectric; } set { _ClusterElectric = value; } }
        [XmlAttribute("GraphMin")]
        public double GraphMin { get { return _GraphMin; } set { _GraphMin = value; } }
        [XmlAttribute("GraphMax")]
        public double GraphMax { get { return _GraphMax; } set { _GraphMax = value; } }
        [XmlAttribute("DiagItems")]
        public string DiagItems { get { return _DiagItems; } set { if (_DiagItems != value) { _DiagItems = value; OnNotify("DiagItems"); } } }
        int _SecBrkControlAddress = 30, _SecBrkControlBit = 2;

        [XmlAttribute("SecBrkControlAddress")]
        public int SecBrkControlAddress { get { return _SecBrkControlAddress; } set { if (_SecBrkControlAddress != value) { _SecBrkControlAddress = value; OnNotify("SecBrkControlAddress"); } } }
        [XmlAttribute("SecBrkControlBit")]
        public int SecBrkControlBit { get { return _SecBrkControlBit; } set { if (_SecBrkControlBit != value) { _SecBrkControlBit = value; OnNotify("SecBrkControlBit"); } } }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnNotify(String parameter)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }
        private void LogAndNotify(String parameter, bool value) // proveriti
        {
            //Log.Write("Motor " + MotorID.ToString() + ": " + parameter + " changed to " + value.ToString());
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(parameter));
            }
        }

        #endregion
    }
}

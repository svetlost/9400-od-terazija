﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StClient
{
    /// <summary>
    /// Interaction logic for SumsPPsPanel.xaml
    /// </summary>
    public partial class SumsPPsPanel : UserControl
    {
        private static volatile SumsPPsPanel instance;
        private static object syncRoot = new Object();

        SumsPPsPanel() { InitializeComponent(); }
        public static SumsPPsPanel Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new SumsPPsPanel();
                    }
                }

                return instance;
            }
        }

        public void AddPanel(CountersSumsPPS obj,int row)
        {            
            Grid.SetColumn(obj, 0);
            Grid.SetRow(obj, row);
            Panelgrid.Children.Add(obj);            
        }
    }

   
}

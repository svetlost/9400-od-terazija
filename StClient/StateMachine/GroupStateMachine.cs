﻿using LogAlertHB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StClient
{
    public class GroupStateMachine
    {
        public GroupStateMachine(GroupLogic _grp)
        {
            grp = _grp;
        }
        GroupLogic grp;
        states _state, _prevState;
        //public states state { get; set; }
        public states state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    _prevState = _state;
                    _state = value;
                    Log.Write(string.Format("Grp {0} state changed from {1} to {2}. ",
                       grp.groupNo, _prevState.ToString(), _state.ToString()), EventLogEntryType.Information);
                    //Debug.WriteLine(string.Format(
                    //    "{0:hh:mm:ss.fff} GRP: {1,6}  STATE: {2,16}  BRK={3,6} MOV={4,6}",
                    //    DateTime.Now, grp.groupNo, _state, grp.Brake, grp.Moving));

                }
            }
        }

        string Message = "";
        Commands commands = new Commands();
        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        public DateTime timeoutTime { get; set; }
        //public bool cmdRstTrip { get; set; }

        // this is done in infinite loop caled MainLoop_Tick
        public void ProcessGroupStateMachine()
        {
            if (grp.MotorsInGroup.Count == 0) return;
            switch (state)
            {
                case states.Idle:
                    //state = states.Idle;
                    if (grp.cmdAuto) state = states.AutoStarting1;
                    else if (grp.cmdJoyStart) state = states.JoyStarting;
                    else if (grp.cmdManStart)
                    {
                        foreach (var motor in grp.MotorsInGroup)
                        {
                            motor.SbcLock = MotorBrake.Unlocked;
                            motor.TV = grp.MV;
                            motor.soloSM.state = states.ManStarted;
                            //motor.cmdManStart = true;
                            //TimedAction.ExecuteWithDelay(new Action(delegate
                            //{ motor.cmdManStart = false; }), TimeSpan.FromMilliseconds(Properties.Settings.Default.SyncInterval));
                        }
                        state = states.ManStarted;
                    }
                    break;
                case states.ManStarted:
                    ResetTimeout(Properties.Settings.Default.TimeoutMan_ms);
                    //foreach (var motor in grp.MotorsInGroup)
                    //    UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
                    state = states.ManStarted2;

                    break;
                case states.ManStarted2:
                    if (grp.Moving) ResetTimeout(Properties.Settings.Default.TimeoutMan_ms);

                    //if (timeoutTime < DateTime.Now) state = states.Stopping1;
                    if (timeoutTime < DateTime.Now) state = states.Stopping2;
                    else if (grp.cmdDown) foreach (var motor in grp.MotorsInGroup) motor.cmdDown = true;
                    else if (grp.cmdUp) foreach (var motor in grp.MotorsInGroup) motor.cmdUp = true;
                    else
                    {
                        foreach (var motor in grp.MotorsInGroup) motor.cmdUp = motor.cmdDown = false;
                        state = states.ManStarted2;
                    }
                    break;
                //case states.ManStarted2:
                //    if (grp.Moving) ResetTimeout(5);
                //    foreach (var motor in grp.MotorsInGroup) motor.cmdUp = motor.cmdDown = false;
                //    //if (timeoutTime < DateTime.Now) state = states.Stopping1;
                //    if (timeoutTime < DateTime.Now) state = states.Stopping2;
                //    else if (grp.cmdDown) state = states.ManNeg;
                //    else if (grp.cmdUp) state = states.ManPos;
                //    else state = states.ManStarted2;
                //    break;
                //case states.ManPos:
                //    foreach (var motor in grp.MotorsInGroup)
                //        motor.cmdUp = true;
                //    //UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.PosDirection, 0, 2);
                //    if (grp.cmdUp == false) state = states.ManStarted;
                //    break;
                //case states.ManNeg:
                //    foreach (var motor in grp.MotorsInGroup)
                //        motor.cmdDown = true;
                //    //UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.NegDirection, 0, 2);
                //    if (grp.cmdDown == false) state = states.ManStarted;
                //    break;
                case states.AutoStarting1:
                    Message = "";
                    foreach (var mot in grp.MotorsInGroup)
                    {
                        mot.TP = grp.SP;
                        mot.TV = grp.SV;
                        if ((Math.Abs(mot.TP - mot.CP) > Properties.Settings.Default.PositionTollerance) &&
                            mot.soloSM.state == states.Idle)
                        {
                            mot.soloSM.state = states.AutoStarting1;
                            Message += string.Format("\n Motor={0}, ID={1}, SP={2} SV={3} CP={4};",
                                mot.Title, mot.MotorID, mot.TP, mot.TV, mot.CP);
                        }
                        //Log.Write("SM(SoloAuto). " + Message, EventLogEntryType.Information);
                    }
                    if (Message != "") Log.Write("SM(GrpAuto). " + Message, EventLogEntryType.Information);
                    state = states.AutoStarting2;
                    break;
                case states.AutoStarting2:
                    if (grp.Moving) state = states.Started;
                    else state = states.AutoStarting2;
                    break;
                case states.Started:
                    if (grp.Brake == true) state = states.Stopped;
                    //else if (grp.cmdStop) state = states.Stopping1;
                    else state = states.Started;
                    break;
                //case states.JoyStarted:
                //    if (grp._joy.Butt1Pressed == true) state = states.JoyButtPressed;
                //    else state = states.JoyStarted;
                //    break;
                //case states.JoyEnded:
                //    grp.MotorsInGroup.ForEach(mot => mot.state = states.Stopping1);
                //    state = states.Stopping2;
                //    break;
                case states.JoyStarting:
                    //grp.MotorsInGroup.ForEach(mot => mot.SbcLock = MotorBrake.Unlocked);
                    foreach (var mot in grp.MotorsInGroup)
                    {
                        mot.SbcLock = MotorBrake.Unlocked;
                        mot.Direction = CanSett.CanCommandStop;
                        UdpTx.UniqueInstance.SendPDO(mot.MotorID, CanSett.CanCommandStop, 100, 8);
                        //mot.soloSM.state = states.Idle;
                    }
                    state = states.JoyButtReleased;
                    ResetTimeout(Properties.Settings.Default.TimeoutJoy_ms);
                    break;
                case states.JoyButtReleased:
                    Debug.WriteLine("{0:hh:mm:ss.fff} {4:hh:mm:ss.fff} raw={1} butt={2} state={3}", DateTime.Now, grp._joy.JV_Raw, grp._joy.Butt1Pressed, state, timeoutTime);

                    if (grp._joy.Butt1Pressed == true) state = states.JoyButtPressed;
                    else if (timeoutTime < DateTime.Now)
                    {
                        foreach (var mot in grp.MotorsInGroup)
                        {
                            UdpTx.UniqueInstance.SendPDO(mot.MotorID, CanSett.CanCommandIdle, 0, 8);
                            mot.Direction = CanSett.CanCommandStop;
                            //mot.soloSM.state = states.Idle;
                        }
                        //foreach(var hoist in Sys.UpDownList) hoist.
                        state = states.Idle;
                    }
                    else state = states.JoyButtReleased;
                    break;
                case states.JoyButtPressed:
                    if (grp._joy.Butt1Pressed == false)
                    {
                        foreach (var mot in grp.MotorsInGroup)
                        {
                            //UdpTx.UniqueInstance.SendSDOWriteRQ(mot.MotorID, CanSett.MvIndex, CanSett.MvSubIndex,
                            //    mot.DefaultMV, SdoPriority.hi);
                            UdpTx.UniqueInstance.SendSDOWriteRQ(mot, CanSett.MvIndex, CanSett.MvSubIndex, mot.DefaultMV, TimeSpan.FromMilliseconds(0));
                            UdpTx.UniqueInstance.SendPDO(mot.MotorID, CanSett.CanCommandStop, 0, 8);
                        }
                        state = states.JoyButtReleased;
                        ResetTimeout(Properties.Settings.Default.TimeoutJoy_ms);
                    }
                    else
                    {
                        //grp._joy.JoyNormalized4(grp);
                        foreach (var mot in grp.MotorsInGroup)
                        {
                            //double joyOut = grp._joy.JoyNormalized3(mot, mot.MaxV);
                            double joyOut = grp._joy.JoyNormalized3(mot, mot.MaxV);
                            if (mot.TV != 0) //1.9.2010. regulator daje error response ako se posalje mv=0
                                UdpTx.UniqueInstance.SendSDOWriteRQ(mot, CanSett.MvIndex, CanSett.MvSubIndex, Math.Abs(mot.TV), TimeSpan.FromMilliseconds(0));
                            //UdpTx.UniqueInstance.SendSDOWriteRQ(mot.MotorID, CanSett.MvIndex, CanSett.MvSubIndex, Math.Abs(mot.TV), SdoPriority.hi);
                            UdpTx.UniqueInstance.SendPDO(mot.MotorID, mot.Direction, 0, 8);
                        }
                        state = states.JoyButtPressed;
                    }
                    break;
                case states.Error1:
                    foreach (var mot in grp.MotorsInGroup.Where(q => q.Brake == false)) mot.soloSM.state = states.Stopping1;
                    state = states.Error2;
                    break;
                case states.Error2:
                    if (grp.cmdRstTrip)
                    {
                        foreach (MotorLogic motor in grp.MotorsInGroup.Where(q => q.Trip == true))
                            UdpTx.UniqueInstance.SendSDOWriteRQ(motor, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 43, TimeSpan.FromMilliseconds(0));
                        //UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 0, SdoPriority.hi);
                        ResetTimeout(1);
                        state = states.Error3;
                    }
                    else state = states.Error2;
                    break;
                case states.Error3:
                    if (grp.Trip == false) state = states.Idle;
                    else if (timeoutTime < DateTime.Now) state = states.Error2;
                    else state = states.Error3;
                    break;

                case states.Stopping1:
                    foreach (var mot in grp.MotorsInGroup.Where(q => q.Brake == false))
                    {
                        mot.soloSM.state = states.Stopping1;
                        //UdpTx.UniqueInstance.SendPDO(mot.MotorID, CanSett.CanCommandStop, 0, 7);
                        //UdpTx.UniqueInstance.SendPDO(mot.MotorID, CanSett.CanCommandIdle, 500, 7);
                    }
                    state = states.Stopping2;
                    break;
                case states.Stopping2:
                    if (grp.Moving == false) state = states.Stopped;
                    else state = states.Stopping2;
                    break;
                case states.Stopped:
                    foreach (var mot in grp.MotorsInGroup) mot.SbcLock = MotorBrake.Unlocked;
                    state = states.Idle;
                    break;
            }

            //if (grp.cmdStop) stopMotors();


            if (grp.Trip == true && !(state == states.Error1 || state == states.Error2 || state == states.Error3))
                state = states.Error1;

            if (state == states.JoyButtPressed || state == states.JoyButtReleased ||
                state == states.JoyStarted || state == states.JoyStarting)
            {
                //Debug.WriteLine("{0:hh:mm:ss.fff} raw={1} butt={2} state={3}", DateTime.Now, grp._joy.JV_Raw, grp._joy.Butt1Pressed, state);

                // JoyManager.readJoystick(grp._joy); -- this is used for directX joystick
                //string tmpS = "";
                //foreach(var mot in grp.MotorsInGroup)
                //{
                //    tmpS += string.Format(" cp={0,10:0.00}");
                //}
                //Debug.WriteLine("xxx:" + tmpS);
            }
        }
        public void stopMotors()
        {
            if (!(state == states.Error1 || state == states.Error2 || state == states.Error3))
                state = states.Stopping1;
        }

        void ResetTimeout(double timemiliSeconds) { timeoutTime = DateTime.Now + TimeSpan.FromMilliseconds(timemiliSeconds); }
    }
}

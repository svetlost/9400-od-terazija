﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LogAlertHB;
using System.Diagnostics;
using System.Windows.Threading;

namespace StClient
{
    /// <summary>
    /// imamo dve vrste manuelnog upravljanja joystik ciju brzinu posle zavrsetka kretanja treba da postavimo na 0 i mv od grupe ili singla gde se to ne radi
    /// 
    /// </summary>
    public class SoloStateMachine
    {
        public SoloStateMachine(MotorLogic _mot) { motor = _mot; }
        MotorLogic motor;

        //double coef = 1;
        //double ManOrJoystick;
        //int ResRefCounter;
        double oldCP;
        Commands commands = new Commands();
        states _state, _prevState;
        public states state
        {
            get { return _state; }
            set
            {
                if (_state != value)
                {
                    _prevState = _state;
                    _state = value;
                    //Debug.WriteLine(string.Format(
                    //    "{0:hh:mm:ss.fff} MOTOR: {1,6}  STATE: {2,16}  SBC:{3,8}  CP={4,8} CV={5,6} BRK={6,6} MOV={7,6}",
                    //    DateTime.Now, motor.MotorID, state, motor.SbcLock, motor.CP, motor.CV, motor.Brake, motor.Moving));

                }
            }
        }

        //public states state { get; set; }

        Properties.Settings Sett = Properties.Settings.Default;
        Properties.CanSettings CanSett = Properties.CanSettings.Default;

        public void ProcessStateMachine()
        {
            switch (state)
            {
                case states.Idle:
                    motor.SbcLock = MotorBrake.Locked;
                    if (motor.cmdAuto)
                    {
                        motor.TP = motor.SP;
                        motor.TV = motor.SV;
                        if (Math.Abs(motor.TP - motor.CP) > 0.01)
                            state = states.AutoStarting1;
                    }
                    else if (motor.cmdRstRef) state = states.ResRef1;
                    else if (motor.cmdManStart)
                    {
                        motor.TV = motor.MV;
                        state = states.ManStarted;
                    }
                    break;

                case states.ManStarted:
                    ResetTimeout(5);
                    motor.SbcLock = MotorBrake.Unlocked;
                    UdpTx.UniqueInstance.SendSDOWriteRQ(motor, CanSett.MvIndex, CanSett.MvSubIndex, motor.TV, TimeSpan.FromMilliseconds(0));
                    //UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.MvIndex, CanSett.MvSubIndex, motor.TV, SdoPriority.hi);
                    UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
                    state = states.ManStarted2;
                    break;
                case states.ManStarted2:
                    if (motor.Moving) ResetTimeout(5);

                    if (timeoutTime < DateTime.Now) state = states.Stopping1;
                    else if (motor.cmdDown) state = states.ManNeg;
                    else if (motor.cmdUp) state = states.ManPos;
                    else state = states.ManStarted2;
                    break;
                case states.ManPos:
                    UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.PosDirection, 0, 8);
                    if (motor.cmdUp == false) state = states.ManStarted;
                    break;
                case states.ManNeg:
                    UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.NegDirection, 0, 8);
                    if (motor.cmdDown == false) state = states.ManStarted;
                    break;
                case states.ManButtReleased:
                    //motor.iMotorIndicators.ManTimeOut = DateTime.Now + TimeSpan.FromSeconds(5);
                    motor.ManTimeOut = DateTime.Now + TimeSpan.FromSeconds(5);
                    UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
                    state = states.ManStarted2;
                    break;
                case states.AutoStarting1:
                    motor.SbcLock = MotorBrake.Unlocked;
                    UdpTx.UniqueInstance.SendSDOWriteRQ(motor, CanSett.SpIndex, CanSett.SpSubIndex, motor.TP, TimeSpan.FromMilliseconds(0));
                    UdpTx.UniqueInstance.SendSDOWriteRQ(motor, CanSett.SvIndex, CanSett.SvSubIndex, motor.TV, TimeSpan.FromMilliseconds(CanSett.SdoProcTime));
                    //UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.SpIndex, CanSett.SpSubIndex, motor.TP, SdoPriority.hi);
                    //UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.SvIndex, CanSett.SvSubIndex, motor.TV, SdoPriority.hi);
                    UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandAuto, CanSett.SdoProcTime * 2, 8);
                    UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, CanSett.SdoProcTime * 12, 8);
                    ResetTimeout(3);//if not started in 3s => timeout=>err
                    state = states.AutoStarting2;
                    break;
                case states.AutoStarting2:
                    if (timeoutTime < DateTime.Now)
                    {
                        ResetTimeout(2);
                        state = states.Error1;
                    }
                    else if (motor.Moving == true) state = states.Started;//todo:dodati timeout?
                    else state = states.AutoStarting2;
                    break;
                case states.Started:
                    motor.SbcLock = MotorBrake.Unlocked;
                    if (motor.Moving)//todo 20140405 prepravljeno zato sto je zaustavljao stimung pre vremena (tj odma na pocetku)
                    {
                        state = states.Started;
                        //motor.StoppingCounter = 0;
                    }
                    else state = states.Stopping2;
                    //else state = states.Stopping2;
                    //else motor.StoppingCounter++;
                    //if (motor.StoppingCounter > 3) motor.state = states.Stopping1;
                    break;

                case states.Stopping1:
                    motor.SbcLock = MotorBrake.Unlocked;
                    ResetTimeout(4);
                    UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
                    state = states.Stopping2;
                    break;
                case states.Stopping2:
                    if (motor.Moving == false || (timeoutTime < DateTime.Now)) state = states.Stopped;
                    else state = states.Stopping2;
                    break;
                case states.Stopped:
                    motor.SbcLock = MotorBrake.Locked;
                    UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, CanSett.SdoProcTime, 8);
                    state = states.Idle;
                    break;
                case states.ResRef1:
                    motor.SbcLock = MotorBrake.Unlocked;
                    //motor.ResRefCounter = 0;
                    oldCP = motor.CP;
                    //UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandRefReset, 1000, 2);
                    //UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, 4000, 2);
                    //UdpTx.UniqueInstance.SendPDO_ResRef(motor.MotorID, CanSett.CanCommandRefReset, 1000, 2);//20170903 res ref terazije. canin2 bit8=256??? a ne bit6=64
                    UdpTx.UniqueInstance.SendPDO_ResRef(motor.MotorID, 256, 1000, 2);//20170903 res ref terazije. canin2 bit8=256??? a ne bit6=64
                    UdpTx.UniqueInstance.SendPDO_ResRef(motor.MotorID, CanSett.CanCommandIdle, 4000, 8);
                    ResetTimeout(4);
                    state = states.ResRef2;
                    break;
                case states.ResRef2:
                    //motor.ResRefCounter++;
                    //if (motor.ResRefCounter > 40)
                    if (timeoutTime < DateTime.Now)
                    {
                        motor.SbcLock = MotorBrake.Locked;
                        state = states.Idle;
                        Log.Write(string.Format("(ResetReference). Motor={0}, MotID={1}, newCP={2}, oldCP{3}",
                            motor.Title, motor.MotorID, motor.CP, oldCP), EventLogEntryType.Information);
                    }
                    else state = states.ResRef2;
                    break;

                case states.Error1:
                    if (motor.Brake == false) state = states.Error3;
                    else
                    {
                        UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandStop, 0, 8);
                        state = states.Error2;
                    }
                    break;
                case states.Error2:
                    if ((motor.Brake == false) || (timeoutTime < DateTime.Now))
                    {
                        UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanSett.CanCommandIdle, 0, 8);
                        state = states.Error3;
                    }
                    else state = states.Error2;
                    break;
                case states.Error3:
                    motor.SbcLock = MotorBrake.Locked;
                    if (motor.cmdRstTrip)
                        UdpTx.UniqueInstance.SendSDOWriteRQ(motor, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 43, TimeSpan.FromMilliseconds(0));
                    //UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 0, SdoPriority.hi);

                    if (motor.Trip == false) state = states.Idle;
                    else state = states.Error3;
                    break;

            }

            //if (motor.MotorID == 1)
            //Debug.WriteLine(string.Format("{0:hh:mm:ss.fff} MOTOR: {1}, STATE: {2}, SBC:{3}", DateTime.Now, motor.MotorID, state, motor.SbcLock));

            if (motor.cmdStop) stopMotor();
        }
        public void cueAuto(CueItem cueItem)
        {
            string Message = "";
            motor.TP = cueItem.SP;
            motor.TV = cueItem.SV;
            if ((Math.Abs(motor.TP - motor.CP) > Properties.Settings.Default.PositionTollerance) &&
                motor.soloSM.state == states.Idle)
            {
                motor.soloSM.state = states.AutoStarting1;
                Message += string.Format("\n Motor={0}, ID={1}, SP={2} SV={3} CP={4};",
                    motor.Title, motor.MotorID, motor.TP, motor.TV, motor.CP);
            }
            Log.Write("SM(CueStart). " + Message, EventLogEntryType.Information);
        }
        public void tripOccured()
        {
            if (!(state == states.Error1 || state == states.Error2 || state == states.Error3))
                state = states.Error1;
        }
        public void tripReset()
        {
            //UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 0, SdoPriority.hi);
            UdpTx.UniqueInstance.SendSDOWriteRQ(motor, CanSett.TripRstIndex, CanSett.TripRstSubIndex, 43, TimeSpan.FromMilliseconds(0));
        }
        public void stopMotor()
        {
            if (!(state == states.Error1 || state == states.Error2 || state == states.Error3))
                state = states.Stopping1;
        }
        public DateTime timeoutTime { get; set; }

        void ResetTimeout(double timeSeconds) { timeoutTime = DateTime.Now + TimeSpan.FromSeconds(timeSeconds); }
    }

    //public class JoysticSender
    //{
    //    DispatcherTimer joystickTimer = new DispatcherTimer();
    //    List<MotorLogic> ml = new List<MotorLogic>();
    //    double coef = 1;
    //    double ManOrJoystick;
    //    public Dispatcher dispacher;



    //    public JoysticSender()
    //    {
    //        Debug.WriteLine("ctor " + DateTime.Now.ToLongTimeString() + "  ms=" + DateTime.Now.Millisecond);
    //        joystickTimer.Interval = TimeSpan.FromMilliseconds(300);
    //        joystickTimer.Tick += new EventHandler(joystickTimer_Tick);
    //        dispacher = joystickTimer.Dispatcher;
    //    }



    //    public void jstart(List<MotorLogic> ml)
    //    {
    //        this.ml = ml;
    //        joystickTimer.Start();
    //    }
    //    public void jstop() { joystickTimer.Stop(); }


    //    void joystickTimer_Tick(object sender, EventArgs e)
    //    {
    //        Debug.WriteLine("timerStarted " + DateTime.Now.ToLongTimeString());
    //        foreach (MotorLogic motor in ml)
    //        {
    //            motor.motorStateType.ManTimeOut = DateTime.Now + TimeSpan.FromSeconds(5);
    //            if (motor.manualVelocityType.MV != motor.PreviousJoystickMV) //ako se ne razlikuju necu da saljem
    //            {
    //                //UdpTx.UniqueInstance.SendPDO(motor.MotorID, CanStructure.CanCommandStop, 0, 8);
    //                coef = motor.calculatesEnabledType.IsSync ? motor.calculatesEnabledType.SyncValue / motor.MaxV : 1;
    //                ManOrJoystick = coef * Math.Abs(motor.manualVelocityType.MV);
    //                //UdpTx.UniqueInstance.SendSDOWriteRQ(motor.MotorID, CanStructure.MvIndex, CanStructure.MvSubIndex, ManOrJoystick, SdoPriority.hi);
    //                //UdpTx.UniqueInstance.SendPDO(motor.MotorID, motor.manualVelocityType.Direction, 0, 2);
    //                Debug.WriteLine(string.Format("JOystick: {2}, MOTOR: {0}, PRAVAC:{1}, STATE: {3}, BRZINA: {4}", motor.MotorID, motor.manualVelocityType.Direction, motor.GroupNo, motor.state, ManOrJoystick));
    //                motor.PreviousJoystickMV = motor.manualVelocityType.MV;
    //                motor.state = states.ManStarted2;
    //            }
    //        }
    //    }



    //}

}

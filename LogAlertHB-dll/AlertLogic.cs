﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAlertHB
{
    public class AlertLogic
    {

        static ObservableCollection<Alerts> _alerts = new ObservableCollection<Alerts>();
        public static ObservableCollection<Alerts> AlertsGet
        { get { return _alerts; } }



        public static void Add(string text)
        {
            try
            {

                _alerts.Add(new Alerts
                {
                    AlertTime = DateTime.Now,
                    AlertText = text
                });

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message); //20150813 sklonio, mislim da ponekad izbacuje exception. staviti ove operacija da idu kroz dispatcher
            }
        }

        [Serializable]
        public class Alerts
        {
            public DateTime AlertTime { get; set; }
            public string AlertText { get; set; }
        }
    }
}

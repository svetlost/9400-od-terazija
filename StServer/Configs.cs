﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

using System.Diagnostics;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using LogAlertHB;
//using StClient;


namespace Server
{
    public class CanChannelConfiguration
    {
        public CanChannelConfiguration() { }       
        public byte Channel = 0;// first channel is ch1, not ch0??????
        public string Description = "Channel_0";
        public uint BitRate = 500000;
        public bool Enabled = true;
        public bool SendSync = true;
    }


   



    public class Configs
    {
        //AlertsLB _lbAlerts;
        public static List<CanChannelConfiguration> CanChannelConfigurationList = new List<CanChannelConfiguration>();
        //public List<MotorLogic> MotorsConfigurationList = new List<MotorLogic>();
       
        string path = Environment.CurrentDirectory + @"\Config\";
        XmlSerializer sss = null;
        public bool CanDriverInitialized = false;
        


        public Configs()
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            //AlertLogic.Add("Loading config file motors_List.xml");

            //sss = new XmlSerializer(typeof(List<MotorLogic>));
            //string motorPath = path + @"motors_List.xml";
            //if (File.Exists(motorPath))
            //{
            //    XmlReader reader = XmlReader.Create(new StreamReader(motorPath));
            //    try
            //    {
            //        MotorsConfigurationList = (List<MotorLogic>)sss.Deserialize(reader);
            //        AlertLogic.Add("Xml Motors load success.");
            //    }
            //    catch (Exception ex)
            //    {
            //        AlertLogic.Add("Xml Motors load exception: " + ex.Message);
            //    }
            //    reader.Close();
            //}
            //else
            //{
            //    AlertLogic.Add("Motors.xml doesn't exist, creating default.");
            //    TextWriter w = new StreamWriter(motorPath);
            //    MotorLogic tempMotorConfiguration = new MotorLogic();
            //    MotorsConfigurationList = new List<MotorLogic>();
            //    MotorsConfigurationList.Add(tempMotorConfiguration);
            //    sss.Serialize(w, MotorsConfigurationList);
            //    w.Close();
            //}


            AlertLogic.Add("Loading config file CanChannelConfiguration.xml.");

            sss = new XmlSerializer(typeof(List<CanChannelConfiguration>));
            string canPath = path + @"CanChannelConfiguration.xml";
            if (File.Exists(canPath))
            {
                XmlReader reader = XmlReader.Create(new StreamReader(canPath));
                try
                {
                    CanChannelConfigurationList = (List<CanChannelConfiguration>)sss.Deserialize(reader);
                    AlertLogic.Add("Xml CanChannelConfiguration load success.");
                }
                catch (Exception ex)
                {
                    AlertLogic.Add("Xml CanChannelConfiguration load exception: " + ex.Message);
                    CanDriverInitialized = false;
                }
                reader.Close();
            }
            else
            {
                AlertLogic.Add("CanChannelConfiguration.xml doesn't exist, creating default.");
                TextWriter w = new StreamWriter(canPath);
                CanChannelConfiguration tempCanChannelConfiguration = new CanChannelConfiguration();
                CanChannelConfigurationList = new List<CanChannelConfiguration>();
                CanChannelConfigurationList.Add(tempCanChannelConfiguration);
                sss.Serialize(w, CanChannelConfigurationList);
                w.Close();
            }
        }

        public virtual void CloseCanDriver() { }

     























    }
}
